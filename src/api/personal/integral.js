import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

//最新积分记录列表
export function getIntegralListAPI(params){
    return request({
        url:`${requestPath.member}/member/score-record/page`,
        method:'get',
        params
    })
}
// 获取用户积分
export function getUserIntegrallNumAPI(){
    return request({
        url:`${requestPath.member}/member/score/member/score-value`,
        method:'get',  
    })
}
//积分设置详情
export function getIntegralSetAPI(){
    return request({
        url:`${requestPath.member}/score-set/list`,
        method:'get',
    })
}
// 更改积分
export function changeIntegralAPI(value,type){
    return request({
        url:`${requestPath.member}/member/score-record/score-recharge?value=${value}&type=${type}`,
        method:'post',
    })
}