import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

//评论列表
export function getEvaluateListAPI(params){
    return request({
        url:`${requestPath.resource}/comment/page/comment`,
        method:'get',
        params
    })
}


// 删除当前评论
export function deleteCommentAPI(params){
    return request({
        url:`${requestPath.resource}/comment/delete/${params}`,
        method:'get',
        // params
    })
}
