import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

//最新活动列表
export function getNewActListAPI(params){
    return request({
        url:`${requestPath.common}/activity/list?num=${params}`,
        method:'get',
    })
}
//最新样书申请列表
export function getNewBookApplyAPI(){
    return request({
        url:`${requestPath.member}/samplebookapply/sample-book/new`,
        method:'get',
    })
}
// 获取公告通知详情
export function getNoticeDetailAPI(params){
    return request({
        url:`${requestPath.common}/bulletin-content/${params}`,
        method:'get',
        params
    })
}

//获取活动表单
export function getApplyFromAPI(params){
    return request({
        url:`${requestPath.common}/form-relation/get/${params}`,
        method:'get',
        params
    })
}
// 保存报名信息
export function saveActApplyAPI(data){
    return request({
        url:`${requestPath.common}/activity-apply/save`,
        method:'post',
        data
    })
}
// 修改报名信息
export function editActApplyAPI(data){
    return request({
        url:`${requestPath.common}/activity-apply/update/activity-apply`,
        method:'post',
        data
    })
}
// 获取活动详情
export function getactivityDetailAPI(params){
    return request({
        url:`${requestPath.common}/activity-apply/get/${params}`,
        method:'get',
        params
    })
}
// 获取用户积分
export function getUserIntegrallNumAPI(){
    return request({
        url:`${requestPath.member}/member/score/member/score-value`,
        method:'get',
        
    })
}

//获取用户签到说明
export function getUserSignInfoAPI(){
    return request({
        url:`${requestPath.member}/member/signed/get`,
        method:'get',
        
    })
}

//用户签到
export function userSignInAPI(){
    return request({
        url:`${requestPath.member}/member/signed/sign`,
        method:'get',
        
    })
}
