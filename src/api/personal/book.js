import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

//样书券申请列表
export function getBookCouponListAPI(params){
    return request({
        url:`${requestPath.member}/member/list/sample-book/record`,
        method:'get',
        params
    })
}

//积分设置详情
export function getIntegralSetAPI(){
    return request({
        url:`${requestPath.member}/score-set/list`,
        method:'get',
    })
}

//  保存反馈信息
export function saveFeedbackInfoAPI(data){
    return request({
        url:`${requestPath.member}/sample-book/feedback/sample-book/feedback-status`,
        method:'post',
        data
    })
}


// 更改积分
export function changeIntegralAPI(value,type){
    return request({
        url:`${requestPath.member}/member/score-record/score-recharge?value=${value}&type=${type}`,
        method:'post',
    })
}
// // 获取公告通知详情
// export function getNoticeDetailAPI(params){
//     return request({
//         url:`${requestPath.common}/bulletin-content/${params}`,
//         method:'get',
//         params
//     })
// }

// //获取活动表单
// export function getApplyFromAPI(params){
//     return request({
//         url:`${requestPath.common}/form-relation/get/${params}`,
//         method:'get',
//         params
//     })
// }
// 确认收货
export function saveReceiveGoodsAPI(data){
    return request({
        url:`${requestPath.member}/samplebookapply/sample-book/wait-feedback-status`,
        method:'post',
        data
    })
}
// // 修改报名信息
// export function editActApplyAPI(data){
//     return request({
//         url:`${requestPath.common}/activity-apply/update/activity-apply`,
//         method:'post',
//         data
//     })
// }
// // 获取活动详情
// export function getactivityDetailAPI(params){
//     return request({
//         url:`${requestPath.common}/activity-apply/get/${params}`,
//         method:'get',
//         params
//     })
// }
// 获取样书券数量
export function getBookCouponNumAPI(){
    return request({
        url:`${requestPath.member}/member/sample-book/num`,
        method:'get',  
    })
}
// 获取用户积分
export function getUserIntegrallNumAPI(){
    return request({
        url:`${requestPath.member}/member/score/member/score-value`,
        method:'get',  
    })
}
