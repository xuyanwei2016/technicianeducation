import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'


//收藏列表
export function getCollectionListAPI(data){
    return request({
        url:`${requestPath.common}/collections/{oid}/page`,
        method:'get',
        params:data
    })
}

// 批量取消收藏
export function batchDeleteColltctionAPI(data){
    return request({
        url:`${requestPath.common}/collections/batch/delete`,
        method:'post',
        data
    })
}
// 单个取消收藏
export function deleteColltctionAPI(data){
    return request({
        url:`${requestPath.common}/collections/delete`,
        method:'post',
        data
    })
}