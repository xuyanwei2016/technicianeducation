import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

//最新活动列表
export function getNewActListAPI(params){
    return request({
        url:`${requestPath.common}/activity/list?num=${params}`,
        method:'get',
    })
}
//活动列表
export function getActListAPI(data){
    return request({
        url:`${requestPath.common}/activity/page`,
        method:'get',
        params:data
    })
}
// 获取活动详情
export function getActivityDetailAPI(params){
    return request({
        url:`${requestPath.common}/activity/${params}`,
        method:'get',
    })
}
//获取活动表单
export function getApplyFromAPI(params){
    return request({
        url:`${requestPath.common}/form-relation/get/${params}`,
        method:'get',
        params
    })
}
// 保存报名信息
export function saveActApplyAPI(data){
    return request({
        url:`${requestPath.common}/activity-apply/save`,
        method:'post',
        data
    })
}
// 修改报名信息
export function editActApplyAPI(data){
    return request({
        url:`${requestPath.common}/activity-apply/update/activity-apply`,
        method:'post',
        data
    })
}
// 获取活动详情
export function getactivityDetailAPI(params){
    return request({
        url:`${requestPath.common}/activity-apply/get/${params}`,
        method:'get',
        params
    })
}


// 获取报名记录列表
export function getEntryRecordAPI(params){
    return request({
        url:`${requestPath.common}/activity-apply/page`,
        method:'get',
        params
    })
}
