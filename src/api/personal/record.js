import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'
//浏览记录
export function getRecordListAPI(data){
    return request({
        url:`${requestPath.common}/record/{oid}/page`,
        method:'get',
        params:data
    })
}

// 批量删除
export function batchDeleteRecordAPI(data){
    return request({
        url:`${requestPath.common}/record/batch/delete`,
        method:'post',
        data
    })
}

// 删除当天
export function batchDeleteTodayRecordAPI(data){
    return request({
        url:`${requestPath.common}/record/batch/delete/today`,
        method:'post',
        data
    })
}
