import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

// //最新活动列表
// export function getNewActListAPI(params){
//     return request({
//         url:`${requestPath.common}/activity/list?num=${params}`,
//         method:'get',
//     })
// }
// //反馈列表
// export function getFeedBackListAPI(data){
//     return request({
//         url:`${requestPath.common}/suggestion/page`,
//         method:'get',
//         params:data
//     })
// }
// //获取反馈表单
// export function getFeedBackFromAPI(params){
//     return request({
//         url:`${requestPath.common}/form-relation/get/${params}`,
//         method:'get',
//         params
//     })
// }

//反馈列表
export function getFeedBackListAPI(data){
    return request({
        url:`${requestPath.common}/feed-back/page`,
        method:'get',
        params:data
    })
}
//用户反馈表单
export function getFeedBackAPI(){
    return request({
        url:`${requestPath.common}/form-relation/get`,
        method:'get',
    })
}
// 保存反馈信息
export function saveFeedBackAPI(data){
    return request({
        url:`${requestPath.common}/feed-back/save`,
        method:'post',
        data
    })
}

