import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

// 列表
export function getListAPI(params){
    return request({
        url:`${requestPath.school}/supervise/statistics/organ/page`,
        method:'get',
        params
    })
}


// 时间列表
export function getTimeListAPI(){
  return request({
      url:`${requestPath.school}/supervise/statistics/get/date/list`,
      method:'get'
  })
}

// 批量下载
export function downLoadAPI(data){
  return request({
      url:`${requestPath.school}/supervise/exportList`,
      method:'post',
      data:data
  })
}



/********************* 详情***********************/

export function getDepListAPI(data) {//部门筛选列表
  return request({
    url: `${requestPath.school}/basic-teaching-plan/department/list`,
    method: 'get',
    params: data
  })
}
  export function getMajListAPI(data) {//专业筛选列表
    return request({
      url: `${requestPath.school}/basic-teaching-plan/major/list`,
      method: 'get',
      params: data
    })
  }
  
  export function getStudentResourceListAPI(data) {//生源列表
    return request({
      url: `${requestPath.school}/dictionarydata/list`,
      method: 'get',
      params: data
    })
  }

export function getDetailsListAPI(data) {//教学计划列表
  return request({
    url: `${requestPath.school}/supervise/statistics/train/check/page`,
    method: 'get',
    params: data
  })
}
  
export function getOrderListAPI(data) {//已订教材列表
  return request({
    url: `${requestPath.school}/supervise/school/basic/get`,
    method: 'get',
    params: data
  })
}


