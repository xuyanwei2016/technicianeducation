import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

//公告通知列表
export function getNotiiceListAPI(data){
    return request({
        url:`${requestPath.common}/bulletin-content/page`,
        method:'get',
        params:data
    })
}

//其他资讯列表
export function getOtherListAPI(data){
    return request({
        url:`${requestPath.common}/news/page`,
        method:'get',
        params:data
    })
}

//站内信列表
export function getStationListAPI(data){
    return request({
        url:`${requestPath.member}/member/member/message-record`,
        method:'get',
        params:data
    })
}
// 获取公告通知详情
export function getNoticeDetailAPI(params){
    return request({
        url:`${requestPath.common}/bulletin-content/${params}`,
        method:'get',
        params
    })
}

// 获取资讯详情
export function getOtherDetailAPI(params){
    return request({
        url:`${requestPath.common}/news/${params}`,
        method:'get',
        params
    })
}
// 获取页脚详情
export function getFooterDetailAPI(params){
    return request({
        url:`${requestPath.common}/footer/${params}`,
        method:'get',
        params
    })
}
// 获取反馈详情
export function getFeedDetailAPI(params){
    return request({
        url:`${requestPath.common}/news/${params}`,
        method:'get',
    })
}

//获取反馈表单
export function getFeedBackFromAPI(params){
    return request({
        url:`${requestPath.common}/form-relation/get/${params}`,
        method:'get',
        params
    })
}
// 保存反馈信息
export function saveFeedBackAPI(data){
    return request({
        url:`${requestPath.common}/suggestion/save`,
        method:'post',
        data
    })
}

// 获取站内消息详情
export function getStationDetailAPI(params){
    return request({
        url:`${requestPath.member}/member/${params}`,
        method:'get',
    })
}

// 修改站内消息状态为已读
export function editMessageStatuskAPI(data){
    return request({
        url:`${requestPath.member}/member/member/message-status`,
        method:'post',
        data
    })
}


