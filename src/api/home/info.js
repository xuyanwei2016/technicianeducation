import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'
// import md5 from 'blueimp-md5'


//首页广告列表
export function getAdListAPI(signValue,num){
    return request({
        url:`${requestPath.common}/apicture/list?signValue=${signValue}&num=${num}`,
        method:'get'
    })
}
// 首页公告通知列表
export function getNoticeListAPI(columnName,num){
    return request({
        url:`${requestPath.common}/bulletin-content/list?columnName=${columnName}&num=${num}`,
        method:'get'
    })
}

//首页技工动态
export function getNewsListAPI(columnName,num){
    return request({
        url:`${requestPath.common}/news/list?columnName=${columnName}&num=${num}`,
        method:'get'
    })
}
// 联盟之声
export function getForumListAPI(columnName,num){
    return request({
        url:`${requestPath.common}/news/list?columnName=${columnName}&num=${num}`,
        method:'get'
    })
}

//论坛之音
export function getUnionListAPI(columnName,num){
    return request({
        url:`${requestPath.common}/news/list?columnName=${columnName}&num=${num}`,
        method:'get'
    })
}
// 样书申请未通过数量
export function getBookReadNumAPI(columnName,num){
    return request({
        url:`${requestPath.member}/samplebookapply/unpassed-num`,
        method:'get'
    })
}