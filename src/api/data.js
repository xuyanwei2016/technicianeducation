
import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

//统计列表
export function getListAPI(data){
    return request({
        url:`${requestPath.school}/supervise/statistics/page`,
        method:'get',
        params:data
    })
}

// 统计数据
export function getDataAPI(data){
    return request({
        url:`${requestPath.school}/supervise/statistics/count`,
        method:'get',
        params:data
    })
}

//机构列表
export function getTrainListAPI(data){
    return request({
        url:`${requestPath.school}/supervise/statistics/school/list`,
        method:'get',
        params:data
    })
}

// 查看页列表
export function getDetailListAPI(data){
    return request({
        url:`${requestPath.school}/supervise/statistics/check/page`,
        method:'get',
        params:data
    })
}


// 部门列表
export function getDeptListAPI(data){
    return request({
        url:`${requestPath.school}/supervise/statistics/department/list`,
        method:'get',
        params:data
    })
}


// 专业列表
export function getMajListAPI(data){
    return request({
        url:`${requestPath.school}/supervise/statistics/major/list`,
        method:'get',
        params:data
    })
}
// 班级列表
export function getClassListAPI(data){
    return request({
        url:`${requestPath.school}/supervise/statistics/class/list`,
        method:'get',
        params:data
    })
}


// // 修改站内消息状态为已读
// export function editMessageStatuskAPI(data){
//     return request({
//         url:`${requestPath.member}/member/member/message-status`,
//         method:'post',
//         data
//     })
// }


