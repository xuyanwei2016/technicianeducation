import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

// 学校列表
export function getSchoolListAPI(){
    return request({
        url:`${requestPath.school}/supervise/school/list`,
        method:'get'
    })
}
// 列表
export function getListAPI(params){
    return request({
        url:`${requestPath.school}/supervise/school/page`,
        method:'get',
        params
    })
}
// 学级列表
export function getAcademicListAPI(params){
    return request({
        url:`${requestPath.school}/supervise/school/academic-level/list?relationId=${params}`,
        method:'get'
    })
}

// 时间列表
export function getTimeListAPI(){
  return request({
      url:`${requestPath.school}/supervise/statistics/get/date/list`,
      method:'get'
  })
}

// 通过拒绝
export function changeStatusAPI(data){
    return request({
        url:`${requestPath.school}/supervise/update/status`,
        method:'post',
        data:data
    })
}
// 批量下载
export function downLoadAPI(data){
  return request({
      url:`${requestPath.school}/supervise/exportList`,
      method:'post',
      data:data
  })
}



/********************* 详情***********************/

export function getDepListAPI(data) {//部门筛选列表
  return request({
    url: `${requestPath.school}/basic-teaching-plan/department/list`,
    method: 'get',
    params: data
  })
}
  export function getMajListAPI(data) {//专业筛选列表
    return request({
      url: `${requestPath.school}/basic-teaching-plan/major/list`,
      method: 'get',
      params: data
    })
  }
  
  export function getStudentResourceListAPI(data) {//生源列表
    return request({
      url: `${requestPath.school}/dictionarydata/list`,
      method: 'get',
      params: data
    })
  }

export function getDetailListAPI(data) {//教学计划列表
  return request({
    url: `${requestPath.school}/supervise/school/basic`,
    method: 'get',
    params: data
  })
}
  
export function getOrderListAPI(data) {//已订教材列表
  return request({
    url: `${requestPath.school}/supervise/school/basic/get`,
    method: 'get',
    params: data
  })
}


