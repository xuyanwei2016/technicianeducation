import request from '@/utils/request';
import {requestPath} from '@/utils/global.js';

/*默认页面广告轮播图*/
export function getApictureAPI(params) {
  return request({
    url: `${requestPath.common}/apicture/list`,
    method: 'get',
    params
  })
}
/*默认页面中的最新商品*/
export function getresourceAPI(params) {
  return request({
    url: `${requestPath.resource}/resource/list/new`,
    method: 'get',
    params
  })
}
/*推荐图书*/
export function getrecommendAPI(params) {
  return request({
    url: `${requestPath.resource}/resource/list/recommend`,
    method: 'get',
    params
  })
}
/*资源列表页面*/
export function getPageAPI(data) {
  return request({
    url: `${requestPath.resource}/resource/page?pageNum=${data.pageNum}&pageSize=${data.pageSize}${data.orderBy?`&orderBy=${data.orderBy}`:''}`,/*&orderBy=${data.orderBy}*/
    method: 'post',
    data
  })
}
/*获取分类下面的*/
export function getdiytypeAPI(params) {
  return request({
    url: `${requestPath.resource}/resource/get/diytype`,
    method: 'get',
    params
  })
}
/*获取培训层次和自定义分类*/
export function getlabelAPI(params) {
  return request({
    url: `${requestPath.resource}/resource/get/label`,
    method: 'get',
    params
  })
}
/*查询电子资源详情*/
export function getEleAPI(data) {
  return request({
    url: `${requestPath.resource}/resource/get/ele?pageNum=${data.pageNum}&pageSize=${data.pageSize}`,/*?oids=${params}*/
    method: 'post',
    data:data.oids
  })
}
/*查看辅助实体资源基础详情(习题册|教参)*/
export function getAidphyAPI(params) {
  return request({
    url: `${requestPath.resource}/resource/get/${params}/aid_phy`,/*?oids=${params}*/
    method: 'get',
  })
}
/*相关资源*/
export function getRelationAPI(params) {
  return request({
    url: `${requestPath.resource}/resource/get/relation`,
    method: 'get',
    params
  })
}
/*关联图书*/
export function getbindingAPI(params) {
  return request({
    url: `${requestPath.resource}/resource/get/binding/phy`,
    method: 'get',
    params
  })
}
/*收藏*/
export function collectionsAPI(data) {
  return request({
    url: `${requestPath.common}/collections/batch/save`,
    method: 'post',
    data
  })
}
/*取消收藏*/
export function cancelCollectionsAPI(data) {
  return request({
    url: `${requestPath.common}/collections/delete`,
    method: 'post',
    data
  })
}

/*获取实体资源详情*/
export function getMainphyAPI(params) {
  return request({
    url: `${requestPath.resource}/resource/get/${params.oid}/main_phy`,
    method: 'get',
  })
}
/*发表评论*/
export function setCommentAPI(data) {
  return request({
    url: `${requestPath.resource}/comment/comment`,
    method: 'post',
    data
  })
}
/*根据资源id查询评论*/
export function getCommentListAPI(params) {
  return request({
    url: `${requestPath.resource}/comment/page/${params.oid}/comment`,
    method: 'get',
    params
  })
}
/*校验是否有绑定资源true：有绑定资源|false：无绑定资源*/
export function isBindAPI(params) {
  return request({
    url: `${requestPath.resource}/resource/check-is-binding/${params.oid}`,
    method: 'get',
  })
}
/*申请样书*/
export function samplebookapplyAPI(data) {
  return request({
    url: `${requestPath.member}/samplebookapply/save`,
    method: 'post',
    data
  })
}
/*查询样书劵数量*/
export function samplebooknumAPI(params) {
  return request({
    url: `${requestPath.member}/member/sample-book/num`,
    method: 'get',
    params
  })
}
/*修改密码*/
export function updatePassAPI(data) {
  return request({
    url: `${requestPath.member}/permissions/update/password`,
    method: 'post',
    data
  })
}
/*获取用户密保问题*/
export function getQuestionAPI(params) {
  return request({
    url: `${requestPath.member}/permissions/get/secret_security`,
    method: 'get',
    params
  })
}
/*获取所有密保问题*/
export function getAllQuestionAPI(params) {
  return request({
    url: `${requestPath.member}/permissions/list/secret_security`,
    method: 'get',
    params
  })
}
/*验证密码*/
export function permissionsAPI(data) {
  return request({
    url: `${requestPath.member}/permissions/check?password=${data.password}`,
    method: 'post',
  })
}
/*保存问题*/
export function saveSecretsecAPI(data) {
  return request({
    url: `${requestPath.member}/permissions/save/secret_security`,
    method: 'post',
    data
  })
}
/*修改保存邮箱*/
export function saveEmailAPI(data) {
  return request({
    url: `${requestPath.member}/permissions/save/email`,
    method: 'post',
    data
  })
}
/*查询个人信息*/
export function getmemberAPI(params) {
  return request({
    url: `${requestPath.member}/member/member-information`,
    method: 'get',
    params
  })
}
/*上传图片*/
export function uploadImgAPI(data) {
  return request({
    url: `${requestPath.file}upload/img`,
    method: 'post',
    data
  })
}
/*修改个人信息*/
export function updateMemberAPI(data) {
  return request({
    url: `${requestPath.member}/member/update/member-information`,
    method: 'post',
    data
  })
}
/*退出登录*/
export function logoutAPI(data) {
  return request({
    url: `${requestPath.member}/permissions/logout`,
    method: 'post',
    data
  })
}
/*设置密码*/
export function resetPasswordAPI(data) {
  return request({
    url: `${requestPath.member}/permissions/reset/password`,
    method: 'post',
    data
  })
}
/*保存历史记录*/
export function saveBatchAPI(data) {
  return request({
    url: `${requestPath.common}/record/batch/save`,
    method: 'post',
    data
  })
}
/*验证用户是否存在*/
export function checkAccountAPI(data) {
  return request({
    url: `${requestPath.member}/permissions/check/account`,
    method: 'post',
    data
  })
}
/*验证密保问题*/
export function secretSecurityAPI(data) {
  return request({
    url: `${requestPath.member}/permissions/check/secret_security`,
    method: 'post',
    data
  })
}
/*验证邮箱问题*/
export function checkEmailAPI(data) {
  return request({
    url: `${requestPath.member}/permissions/check/email`,
    method: 'post',
    data
  })
}
/*增加下载数*/
export function addDownloadAPI(data) {
  return request({
    url: `${requestPath.resource}/resource/update/${data}/download`,
    method: 'post',
  })
}
/*增加点击数*/
export function addHitAPI(data) {
  return request({
    url: `${requestPath.resource}/resource/update/${data}/hit`,
    method: 'post',
  })
}
/*搜索框自动补全*/
export function getsuggestAPI(params) {
  return request({
    url: `${requestPath.resource}/resource/get/suggest`,
    method: 'get',
    params
  })
}
/*教材自动补全*/
export function getBookSuggestAPI(params) {
  return request({
    url: `${requestPath.resource}/resource/get/search`,
    method: 'get',
    params
  })
}

/*下载资源*/
export function uploadFile(params) {
  return request({
    url: `${requestPath.file}download?filePath=${params}`,
    method: 'get',
  })
}
/*查看用户是否存在*/
export function haveAccount(params) {
  return request({
    url: `${requestPath.member}/permissions/account?account=${params}`,
    method: 'get',
  })
}
/*邮箱验证码*/
export function getEmailCode(data) {
  return request({
    url: `${requestPath.third}/sdksms/send/check?toAddress=${data}`,
    method: 'post',
  })
}
// 推荐图书
export function addRecommendAPI(data) {
  return request({
    url: `${requestPath.school}/book-recommend/recommend`,
    method: 'post',
    data
  })
}

// 重新推荐推荐图书
export function resetAddRecommendAPI(data) {
  return request({
    url: `${requestPath.school}/book-recommend/renew/recommend`,
    method: 'post',
    data
  })
}
// 覆盖推荐图书

export function coverRecommendAPI(data) {
  return request({
    url: `${requestPath.school}/book-recommend/chose`,
    method: 'post',
    data
  })
}
// 初审创建新推荐
export function examineRecommendAPI(data) {
  return request({
    url: `${requestPath.school}/book-recommend/create-recommend`,
    method: 'post',
    data
  })
}

export function getBookListAPI(data) {//校本库列表
  return request({
    url: `${requestPath.school}/schoolased-library/page`,
    method: 'get',
    params: data
  })
}

export function getPressListAPI(params) {//获取出版社列表
  return request({
    url: `${requestPath.resource}/resource/publisher/page`,
    method: 'get',
  })
}
