import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function getAcademicListAPI(data) {//学级列表
  return request({
    url: `${requestPath.school}/basic-teaching-plan/academic-level/list`,
    method: 'get',
    params: data
  })
}

export function getDepListAPI(data) {//部门筛选列表
  return request({
    url: `${requestPath.school}/basic-teaching-plan/department/list`,
    method: 'get',
    params: data
  })
}
  export function getMajListAPI(data) {//专业筛选列表
    return request({
      url: `${requestPath.school}/basic-teaching-plan/major/list`,
      method: 'get',
      params: data
    })
  }
  
  export function getStudentResourceListAPI(data) {//生源列表
    return request({
      url: `${requestPath.school}/dictionarydata/list`,
      method: 'get',
      params: data
    })
  }

export function getListAPI(data) {//教学计划列表
    return request({
      url: `${requestPath.school}/supervise/school/basic`,
      method: 'get',
      params: data
    })
  }
// export function batchDeleteAPI(data) { //批量删除
//     return request({
//       url: `${requestPath.school}/basic-teaching-plan/batch/delete`,
//       method: 'post',
//       data
//     })
//   }

  // export function excelImportAPI(data) { //导入基础教学计划
  //   return request({
  //     url: `${requestPath.school}/basic-teaching-plan/import`,
  //     method: 'post',
  //     data
  //   })
  // }
export function submitDataAPI(params) { //提交
  return request({
    url: `${requestPath.school}/supervise/batch/submit?academicLevel=${params}`,
    method: 'get',
  })
}
  