import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

// 获取表格数据
export function getListAPI(params) {
  return request({
    url: `${requestPath.member}/member/message-record/page`,
    method: 'get',
    params
  })
}

// 新增和编辑
export function addAPI(data) {
  return request({
    url: `${requestPath.member}/member/message-record/save`,
    method: 'post',
    data
  })
}

// 删除
export function deleteAPI(data) {
  return request({
    url: `${requestPath.member}/member/message-record/batch/delete`,
    method: 'post',
    data
  })
}

// 查询
export function viewAPI(data) {
  return request({
    url: `${requestPath.member}/member/message-record/${data}`,
    method: 'get',
    data
  })
}






export function getMemberListAPI(params) {
  return request({
    url: `${requestPath.member}/member/message-record/page/member`,
    method: 'get',
    params
  })
}

// 指定会员查询
export function getMemberQueryAPI(params) {
  return request({
    url: `${requestPath.member}/member/message-record/page/member`,
    method: 'get',
    params
  })
}

export function departListAPI(params) {//部门列表
    return request({
        url: `${requestPath.member}/department/page`,
        method: 'get',
        params
    })
}
