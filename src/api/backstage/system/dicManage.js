import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

// 获取字典列表
export function getListAPI(params) {
  return request({
    url: `${requestPath.school}/dictionarydata/page`,
    method: 'get',
    params
  })
}

// 新增字典
export function saveAPI(data) {
  return request({
    url: `${requestPath.school}/dictionarydata/save`,
    method: 'post',
    data
  })
}

// 修改字典
export function editAPI(data) {
    return request({
      url: `${requestPath.school}/dictionarydata/update`,
      method: 'post',
      data
    })
  }


// 查询
export function detailsAPI(data) {
  return request({
    url: `${requestPath.school}/dictionarydata/${data}`,
    method: 'get',
    data
  })
}


// 删除
export function deleteAPI(data) {
    return request({
      url: `${requestPath.school}/dictionarydata/batch/delete`,
      method: 'post',
      data
    })
  }


