import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'


export function getBookNumAPI() {//样书券数量
    return request({
        url: `${requestPath.member}/school-sample-book/page`,
        method: 'get',
    })
}
export function getListAPI(params) {//样书券列表
    return request({
        url: `${requestPath.member}/samplebookvoucher/page`,
        method: 'get',
        params
    })
}




export function bookExtendAPI(data) { //样书券发放
    return request({
        url: `${requestPath.member}/samplebookvoucher/sendsamplebook`,
        method: 'post',
        data
    })
}

export function getMemberListAPI(params) {//会员列表
    return request({
        url: `${requestPath.member}/teacher/page`,
        method: 'get',
        params
    })
}



export function departListAPI(params) {//部门列表
    return request({
        url: `${requestPath.member}/department/page`,
        method: 'get',
        params
    })
}
export function roleListAPI(data) {//角色列表
    return request({
        url: `${requestPath.member}/role/page`,
        method: 'get',
        params:data
    })
}

export function getBookListAPI(params) {//样书申请列表
    return request({
        url: `${requestPath.member}/samplebookapply/page`,
        method: 'get',
        params
    })
}

// export function batchToExamineAPI(data) { //批量审核申请
//     return request({
//         url: `${requestPath.member}/samplebookapply/batch/update/status`,
//         method: 'post',
//         data
//     })
// }

export function detailsBookApplyAPI(params) { //样书申请详情
    return request({
        url: `${requestPath.member}/samplebookapply/${params}`,
        method: 'get',
        params
    })
}








// export function getRelationListAPI(params) {//关联表单列表
//     return request({
//         url: `${requestPath.member}/form-relation/page`,
//         method: 'get',
//         params
//     })
// }


// export function setDeliveBookAPI(data) { //设置发货
//     return request({
//         url: `${requestPath.member}/samplebookapply/batch/update/send-book`,
//         method: 'post',
//         data
//     })
// }




// export function changeSortAPI(data) { //修改排序
//     return request({
//         url: `${requestPath.member}/form-relation/update/sort`,
//         method: 'post',
//         data
//     })
// }



// export function detailsRelationAPI(params) { //关联表单详情
//     return request({
//         url: `${requestPath.member}/form-relation/${params}`,
//         method: 'get',
//         params
//     })
// }

// export function saveAPI(data) { //添加表单
//     return request({
//         url: `${requestPath.member}/form/save`,
//         method: 'post',
//         data
//     })
// }

// export function saveRelationAPI(data) { //添加关联表单
//     return request({
//         url: `${requestPath.member}/form-relation/save`,
//         method: 'post',
//         data
//     })
// }

// export function editAPI(data) { //修改表单
//     return request({
//         url: `${requestPath.member}/form/update`,
//         method: 'post',
//         data
//     })
// }

// export function editRelationAPI(data) { //修改关联表单
//     return request({
//         url: `${requestPath.member}/form-relation/update`,
//         method: 'post',
//         data
//     })
// }


// export function getMaxSortAPI(params) { //获取最大排序号
//     return request({
//         url: `${requestPath.member}/form-relation/get/${params}/max-sort`,
//         method: 'get',
//         params
//     })
// }

// export function detailsFieldRelationAPI(params) { //关联字段详情
//     return request({
//         url: `${requestPath.member}/form-relation/${params}`,
//         method: 'get',
//         params
//     })
// }

export function toExamineAPI(data) { 
    return request({
        url: `${requestPath.member}/samplebookapply/save/text`,
        method: 'post',
        data
    })
}

