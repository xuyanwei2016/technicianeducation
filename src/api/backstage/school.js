import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function getSchoolAPI(data) {//获取学校详情
  return request({
    url: `${requestPath.school}/school/get`,
    method: 'get',
    /*params: data*/
  })
}




export function updateAPI(data) { //修改学校
    return request({
      url: `${requestPath.school}/school/update`,
      method: 'post',
      data
    })
  }


