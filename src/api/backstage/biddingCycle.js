import request from '@/utils/request'
import {requestPath} from '@/utils/global.js'

export function getListAPI(data) {//获取征订周期列表
  return request({
    url: `${requestPath.school}/bidding-cycle/page`,
    method: 'get',
    params: data
  })
}

export function updateAPI(data) { //修改
  return request({
    url: `${requestPath.school}/bidding-cycle/update`,
    method: 'post',
    data
  })
}

export function getMaxTimeAPI(data) {//获取上一个征订季的时间
  return request({
    url: `${requestPath.school}/bidding-cycle/max/${data}`,
    method: 'get',
    /* params: data*/
  })
}

export function saveAPI(data) { //新增
  return request({
    url: `${requestPath.school}/bidding-cycle/save`,
    method: 'post',
    data
  })
}
/*GET /fg/bidding-cycle/{oid}*/
export function getDetailAPI(data) {//获取详情
  return request({
    url: `${requestPath.school}/bidding-cycle/${data}`,
    method: 'get',
    /* params: data*/
  })
}

