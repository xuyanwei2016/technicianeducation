import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function getListAPI(data) {//获取流程列表
  return request({
    url: `${requestPath.school}/bidding-flow/page`,
    method: 'get',
  })
}
export function addAPI(data) { //新增征订部门
    return request({
      url: `${requestPath.school}/bidding-flow/add`,
      method: 'post',
      data
    })
  }
export function deleteAPI(data) {//删除
  return request({
    url: `${requestPath.school}/bidding-flow/delete`,
    method: 'get',
    params: data
  })
}
export function saveAPI(data) { //新增征订流程
  return request({
    url: `${requestPath.school}/bidding-flow/save`,
    method: 'post',
    data
  })
}
export function updateAPI(data) { //修改征订流程
  return request({
    url: `${requestPath.school}/bidding-flow/update`,
    method: 'post',
    data
  })
}
export function lookAPI(data) {//查看该步骤已绑定用户
  return request({
    url: `${requestPath.school}/bidding-flow/${data}`,
    method: 'get',
  })
}

export function departmentAPI(data) {//获取部门列表定用户
  return request({
    url: `${requestPath.member}/department/all`,
    method: 'get',
  })
}
export function flowMemberPageAPI(data) { //用户关系列表
  return request({
    url: `${requestPath.school}/flow-member/page`,
    method: 'get',
    params: data
  })
}
export function rolePageAPI(data) {//角色列表
  return request({
    url: `${requestPath.member}/role/page`,
    method: 'get',
    params: data
  })
}

export function bindUserAPI(data) { //绑定用户
  return request({
    url: `${requestPath.school}/bidding-flow/add/user`,
    method: 'post',
    data
  })
}
export function biddingDlowAPI(data) {//新增征订部门列表
  return request({
    url: `${requestPath.school}/bidding-flow/list`,
    method: 'get',
  })
}

export function editNameAPI(data) { //修改名称
  return request({
    url: `${requestPath.school}/bidding-flow/edit`,
    method: 'post',
    data
  })
}
