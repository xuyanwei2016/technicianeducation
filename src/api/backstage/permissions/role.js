import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'
// 角色列表
export function getListAPI(params) {
  return request({
    url: `${requestPath.member}/role/page`,
    method: 'get',
    params
  })
}

// 批量删除
export function batchDeleteAPI(data) {
  return request({
    url: `${requestPath.member}/role/batch/delete`,
    method: 'post',
    data
  })
}
// 更新角色
export function updateAPI(data) {
  return request({
    url: `${requestPath.member}/role/update`,
    method: 'post',
    data
  })
}

// 添加角色
export function addAPI(data) {
  return request({
    url: `${requestPath.member}/role/save`,
    method: 'post',
    data
  })
}
// 获取详情
export function getDetailAPI(params) {
    return request({
      url: `${requestPath.member}/role/${params}`,
      method: 'get'
    })
  }
export function authorityAPI(data) {
  return request({
    url: `${requestPath.member}/role/saveOperatepowerToRole`,
    method: 'post',
    data
  })
}
// 获取模块列表
export function getModuleAPI() {
  return request({
    url: `${requestPath.member}/modulepower/page`,
    method: 'get'
  })
}

export function getRoleBtnAPI(params) {
  return request({
    url: `${requestPath.member}/operatepower/list/role/${params}`,
    method: 'get'
  })
}
// 获取按钮列表
export function getBtnsAPI() {
  return request({
    url: `${requestPath.member}/operatepower/page`,
    method: 'get'
  })
}

// export function isExistAPI(params) {
//   return request({
//     url: `${requestPath.member}/role/isExist/roleName`,
//     method: 'get',
//     params
//   })
// }
// 获取模块列表
export function getModuleListAPI() {
    return request({
      url: `${requestPath.member}/modulepower/page`,
      method: 'get'
    })
  }