import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'
export function getListAPI(params) {
  return request({
    url: `${requestPath.member}/operatepower/list?mpid=${params}`,
    method: 'get'
  })
}

export function getModuleAPI() {
  return request({
    url: `${requestPath.member}/modulepower/page`,
    method: 'get'
  })
}

export function addAPI(data) {
  return request({
    url: `${requestPath.member}/operatepower/save`,
    method: 'post',
    data
  })
}

export function updateAPI(data) {
  return request({
    url: `${requestPath.member}/operatepower/update`,
    method: 'post',
    data
  })
}

export function deleteAPI(data) {
  return request({
    url: `${requestPath.member}/operatepower/batch/delete`,
    method: 'post',
    data
  })
}
