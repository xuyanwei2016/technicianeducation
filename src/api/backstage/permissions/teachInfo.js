import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function getListAPI(data) {//用户列表
  return request({
    url: `${requestPath.member}/teacher/page`,
    method: 'get',
    params: data
  })
}


export function batchDeleteAPI(data) { //批量删除
    return request({
      url: `${requestPath.member}/teacher/batch/delete`,
      method: 'post',
      data
    })
  }

  export function changeStatusAPI(data) { //批量启用，禁用
    return request({
      url: `${requestPath.member}/teacher/batch/update/status`,
      method: 'post',
      data
    })
  }



//修改用户
export function editAPI(data) {
  return request({
    url: `${requestPath.member}/teacher/update`,
    method: 'post',
    data
  })
}

export function saveAPI(data) { //添加用户
    return request({
      url: `${requestPath.member}/teacher/save`,
      method: 'post',
      data
    })
  }


export function detailsAPI(params) {//获取用户详情
    return request({
      url: `${requestPath.member}/teacher/${params}`,
      method: 'get',
      params
    })
  }

  export function departListAPI(data) {//部门列表
    return request({
      url: `${requestPath.member}/department/page`,
      method: 'get',
      params: data
    })
  }
  
  export function roleListAPI(data) {//角色列表
    return request({
      url: `${requestPath.member}/role/page`,
      method: 'get',
      params: data
    })
  }

  export function resetPasswordAPI(data) { //重置密码
    return request({
      url: `${requestPath.member}/member/update/password`,
      method: 'post',
      data
    })
  }

  
  // 导入用户
  export function excelImportAPI(data) { 
    return request({
      url: `${requestPath.member}/teacher/import`,
      method: 'post',
      data
    })
  }
  