import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function getListAPI(data) {//用户列表
  return request({
    url: `${requestPath.member}/user/page`,
    method: 'get',
    params: data
  })
}

export function departListAPI(data) {//部门列表
    return request({
      url: `${requestPath.member}/department/all`,
      method: 'get',
      params: data
    })
  }

  export function roleListAPI(data) {//角色列表
    return request({
      url: `${requestPath.member}/role/page`,
      method: 'get',
      params: data
    })
  }

  export function noUserListAPI(data) {//没权限用户列表
    return request({
      url: `${requestPath.member}/user/list`,
      method: 'get',
      params: data
    })
  }


//修改用户
export function editAPI(data) {
  return request({
    url: `${requestPath.member}/user/update`,
    method: 'post',
    data
  })
}

export function choiseSaveAPI(data) { //选择用户
    return request({
      url: `${requestPath.member}/user/save`,
      method: 'post',
      data
    })
  }


export function saveAPI(data) { //添加用户
    return request({
      url: `${requestPath.member}/teacher/save`,
      method: 'post',
      data
    })
  }

export function getDetailAPI(params) {//获取用户详情
    return request({
      url: `${requestPath.member}/user/${params}`,
      method: 'get',
      params
    })
  }


  // 导入用户
  export function batchDeleteAPI(data) {
    return request({
      url: `${requestPath.member}/user/batch/delete`,
      method: 'post',
      data
    })
  }
