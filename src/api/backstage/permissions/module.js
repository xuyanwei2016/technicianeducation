import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function getListAPI() {
  return request({
    url: `${requestPath.member}/modulepower/page`,
    method: 'get'
  })
}

export function deleteAPI(data) {
  return request({
    url: `${requestPath.member}/modulepower/batch/delete`,
    method: 'post',
    data
  })
}

export function updateAPI(data) {
  return request({
    url: `${requestPath.member}/modulepower/update`,
    method: 'post',
    data
  })
}

export function addAPI(data) {
  return request({
    url: `${requestPath.member}/modulepower/save`,
    method: 'post',
    data
  })
}
