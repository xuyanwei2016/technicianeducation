import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'


export function getPayListAPI(data) {//支付详情
    return request({
        url: `${requestPath.school}/pay/set/page`,
        method: 'get',
        params: data
    })
}

// export function savePaySetAPI(data) { //支付设置
//     return request({
//         url: `${requestPath.school}/pay/set/save`,
//         method: 'post',
//         data
//     })
// }




