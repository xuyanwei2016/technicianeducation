import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function getListAPI(data) {//获取供应商列表
  return request({
    url: `${requestPath.school}/supplier/page`,
    method: 'get',
    params: data
  })
}
export function addAPI(data) { //新增
    return request({
      url: `${requestPath.school}/supplier/save`,
      method: 'post',
      data
    })
  }
  export function updateAPI(data) { //修改
    return request({
      url: `${requestPath.school}/supplier/update`,
      method: 'post',
      data
    })
  }
export function batchDeteleAPI(data) { //批量删除
  return request({
    url: `${requestPath.school}/supplier/batch/delete`,
    method: 'post',
    data
  })
}
// export function getSupplierListAPI(data) {//供应商下拉列表
//   return request({
//     url: `${requestPath.school}/supplier/all`,
//     method: 'get',
//     params: data
//   })
// }
export function getSupplierListAPI() {//供应商下拉列表
  return request({
    url: `${requestPath.school}/supplier/get`,
    method: 'get',
  })
}
export function exportListAPI(data) {//导出供应商列表
  return request({
    url: `${requestPath.school}/supplier/export`,
    method: 'get',
    params: data
  })
}
/*GET /fg/supplier/{oid}*/
export function getDetailsAPI(data) {//查询供应商详情
  return request({
    url: `${requestPath.school}/supplier/${data}`,
    method: 'get',
  })
}
/*GET /fg/supplier/get/{oid}*/
export function getDetails2API(data) {//点击列表数据是显示
  return request({
    url: `${requestPath.school}/supplier/get/${data}`,
    method: 'get',
  })
}
