import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function getListAPI(data) {//部门列表
  return request({
    url: `${requestPath.member}/department/page`,
    method: 'get',
    params: data
  })
}


export function batchDeleteAPI(data) { //批量删除
    return request({
      url: `${requestPath.member}/department/batch/delete`,
      method: 'post',
      data
    })
  }
  export function getMaxSortAPI() {//部门最大排序号
    return request({
      url: `${requestPath.member}/department/get/maxOrderNum`,
      method: 'get',
    })
  }

//修改部门
export function updateAPI(data) {
  return request({
    url: `${requestPath.member}/department/update`,
    method: 'post',
    data
  })
}

export function saveAPI(data) { //添加部门
    return request({
      url: `${requestPath.member}/department/save`,
      method: 'post',
      data
    })
  }


export function detailsAPI(params) {//获取部门详情
    return request({
      url: `${requestPath.member}/department/${params}`,
      method: 'get',
      params
    })
  }

  export function excelImportAPI(data) { //导入部门
    return request({
      url: `${requestPath.member}/department/import`,
      method: 'post',
      data
    })
  }
  export function printAPI(data) { //导出部门
    return request({
      url: `${requestPath.member}/department/export`,
      method: 'get',
      params:data
    })
  }
  