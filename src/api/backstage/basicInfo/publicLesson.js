import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function getListAPI(data) {//公共课列表
  return request({
    url: `${requestPath.school}/pulic-lesson/page`,
    method: 'get',
    params: data
  })
}

export function getAcademicListAPI(data) {//学级列表
  return request({
    url: `${requestPath.school}/basic-teaching-plan/academic-level/list`,
    method: 'get',
    params: data
  })
}
export function getDepartmentListAPI(data) {//部门列表
  return request({
    url: `${requestPath.school}/basic-teaching-plan/department/list`,
    method: 'get',
    params: data
  })
}

export function getMajorListAPI(data) {//专业列表
  return request({
    url: `${requestPath.school}/major/page`,
    method: 'get',
    params: data
  })
}


export function batchDeleteAPI(data) { //批量删除
    return request({
      url: `${requestPath.school}/pulic-lesson/batch/delete`,
      method: 'post',
      data
    })
  }
  export function getTrainListAPI(data) {//培养层次列表
    return request({
      url: `${requestPath.school}/basic-teaching-plan/page`,
      method: 'get',
      params: data
    })
  }

  export function getDetailAPI(params) {//获取公共课详情
    return request({
      url: `${requestPath.school}/pulic-lesson/${params}`,
      method: 'get',
      params
    })
  }

export function saveAPI(data) { //添加公共课
    return request({
      url: `${requestPath.school}/pulic-lesson/save`,
      method: 'post',
      data
    })
  }

  export function getBookListAPI(data) {//校本库列表
    return request({
      url: `${requestPath.school}/schoolased-library/page`,
      method: 'get',
      params: data
    })
  }
  export function updateAPI(data) { //修改公共课
    return request({
      url: `${requestPath.school}/pulic-lesson/update`,
      method: 'post',
      data
    })
  }
  export function excelImportAPI(data,type) { //导入公共课
    return request({
      url: `${requestPath.school}/pulic-lesson/import?type=${type}`,
      method: 'post',
      data
    })
  }