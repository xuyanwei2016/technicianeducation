import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function getAcademicListAPI(data) {//学级列表
  return request({
    url: `${requestPath.school}/student/academic-level/list`,
    method: 'get',
    params: data
  })
}
export function getAddAcademicListAPI(data) {//添加页学级列表
  return request({
    url: `${requestPath.school}/class-management/academic-level/list`,
    method: 'get',
    params: data
  })
}
export function getDepartmentListAPI(data) {//部门列表
  return request({
    url: `${requestPath.member}/department/down/all`,
    method: 'get',
    params: data
  })
}
export function getDepListAPI(data) {//部门筛选列表
  return request({
    url: `${requestPath.school}/student/department/list`,
    method: 'get',
    params: data
  })
}
export function getLastDetAPI(params) {//获取上一次添加详情
  return request({
    url: `${requestPath.school}/student/get`,
    method: 'get',
    params
  })
}
export function getMajorListAPI(data) {//专业列表
  return request({
    url: `${requestPath.school}/major/page`,
    method: 'get',
    params: data
  })
}
export function getMajListAPI(data) {//专业筛选列表
  return request({
    url: `${requestPath.school}/student/major/list`,
    method: 'get',
    params: data
  })
}
export function getTrainListAPI(data) {//培养层次列表
  return request({
    url: `${requestPath.school}/basic-teaching-plan/page`,
    method: 'get',
    params: data
  })
}


export function getClassListAPI(data) {//班级列表
  return request({
    url: `${requestPath.school}/class-management/page`,
    method: 'get',
    params: data
  })
}


export function getListAPI(data) {//学生列表
  return request({
    url: `${requestPath.school}/student/page`,
    method: 'get',
    params: data
  })
}


export function batchDeleteAPI(data) { //批量删除
    return request({
      url: `${requestPath.school}/student/batch/delete`,
      method: 'post',
      data
    })
  }


//修改资源
export function updateAPI(data) {
  return request({
    url: `${requestPath.school}/student/update`,
    method: 'post',
    data
  })
}

export function saveAPI(data) { //添加资源
    return request({
      url: `${requestPath.school}/student/save`,
      method: 'post',
      data
    })
  }


export function getDetailAPI(params) {//获取资源详情
    return request({
      url: `${requestPath.school}/student/${params}`,
      method: 'get',
      params
    })
  }

  export function excelImportAPI(data) { //导入学生
    return request({
      url: `${requestPath.school}/student/import`,
      method: 'post',
      data
    })
  }
//   export function printAPI(data) { //导出学生
//     return request({
//       url: `${requestPath.school}/student/export`,
//       method: 'get',
//       params:data
//     })
//   }
  