import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function getDepartmentListAPI(data) {//部门列表
  return request({
    url: `${requestPath.school}/major/department/list`,
    method: 'get',
    params: data
  })
}

export function getDepListAPI(data) {//部门添加页列表
  return request({
    url: `${requestPath.member}/department/down/all`,
    method: 'get',
    params: data
  })
}

export function getListAPI(data) {//专业列表
    return request({
      url: `${requestPath.school}/major/page`,
      method: 'get',
      params: data
    })
  }
export function batchDeleteAPI(data) { //批量删除
    return request({
      url: `${requestPath.school}/major/batch/delete`,
      method: 'post',
      data
    })
  }

  export function getLastDetAPI(params) {//获取上一次添加详情
    return request({
      url: `${requestPath.school}/major/get`,
      method: 'get',
      params
    })
  }

//专业详情
export function getDetailAPI(params) {
    return request({
    url: `${requestPath.school}/major/${params}`,
    method: 'get',
    params
})
}

export function saveAPI(data) { //添加专业
    return request({
      url: `${requestPath.school}/major/save`,
      method: 'post',
      data
    })
  }

  export function updateAPI(data) { //修改专业
    return request({
      url: `${requestPath.school}/major/update`,
      method: 'post',
      data
    })
  }

export function getMajorAPI(params) {
  return request({
    url: `${requestPath.school}/major-type/get/list`,
    method: 'get',
  })
}
export function getMajorDetailAPI(params) {
  return request({
    url: `${requestPath.school}/major-type/${params}`,
    method: 'get',
  })
}
export function excelImportAPI(data) { //导入专业
  return request({
    url: `${requestPath.school}/major/import`,
    method: 'post',
    data
  })
}

//   export function excelImportAPI(data) { //导入部门
//     return request({
//       url: `${requestPath.member}/department/import`,
//       method: 'post',
//       data
//     })
//   }
//   export function printAPI(data) { //导出部门
//     return request({
//       url: `${requestPath.member}/department/export`,
//       method: 'get',
//       params:data
//     })
//   }
