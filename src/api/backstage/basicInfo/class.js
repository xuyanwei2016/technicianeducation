import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function getClassAcademicListAPI(data) {//班级列表
    return request({
      url: `${requestPath.school}/class-management/academic-level/list`,
      method: 'get',
      params: data
    })
  }
  export function getAcademicListAPI(data) {//学级列表
    return request({
      url: `${requestPath.school}/class-management/academic-level/list`,
      method: 'get',
      params: data
    })
  }
  export function getAddAcademicListAPI(data) {//添加页学级列表
    return request({
      url: `${requestPath.school}/basic-teaching-plan/academic-level/list`,
      method: 'get',
      params: data
    })
  }

export function getDepartmentListAPI(data) {//部门列表
  return request({
    url: `${requestPath.member}/department/down/all`,
    method: 'get',
    params: data
  })
}
export function getDepListAPI(data) {//部门筛选列表
  return request({
    url: `${requestPath.school}/class-management/department/list`,
    method: 'get',
    params: data
  })
}
export function getMajorListAPI(data) {//专业列表
    return request({
      url: `${requestPath.school}/major/page`,
      method: 'get',
      params: data
    })
  }
  export function getMajListAPI(data) {//专业筛选列表
    return request({
      url: `${requestPath.school}/class-management/major/list`,
      method: 'get',
      params: data
    })
  }

export function getListAPI(data) {//班级列表
    return request({
      url: `${requestPath.school}/class-management/page`,
      method: 'get',
      params: data
    })
  }
export function batchDeleteAPI(data) { //批量删除
    return request({
      url: `${requestPath.school}/class-management/batch/delete`,
      method: 'post',
      data
    })
  }



  

  export function getDetailAPI(params) {//获取班级详情
    return request({
      url: `${requestPath.school}/class-management/${params}`,
      method: 'get',
      params
    })
  }
  export function getLastDetAPI(params) {//获取上一次添加详情
    return request({
      url: `${requestPath.school}/class-management/get`,
      method: 'get',
      params
    })
  }

export function saveAPI(data) { //添班级
    return request({
      url: `${requestPath.school}/class-management/save`,
      method: 'post',
      data
    })
  }

  export function updateAPI(data) { //修改班级
    return request({
      url: `${requestPath.school}/class-management/update`,
      method: 'post',
      data
    })
  }

  export function getTrainListAPI(data) {//培养层次列表
    return request({
      url: `${requestPath.school}/basic-teaching-plan/page`,
      method: 'get',
      params: data
    })
  }

  export function excelImportAPI(data) { //导入班级
    return request({
      url: `${requestPath.school}/class-management/import`,
      method: 'post',
      data
    })
  }