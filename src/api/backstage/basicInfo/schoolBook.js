import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function getListAPI(data) {//校本库列表
  return request({
    url: `${requestPath.school}/schoolased-library/page`,
    method: 'get',
    params: data
  })
}


export function batchDeleteAPI(data) { //批量删除
    return request({
      url: `${requestPath.school}/schoolased-library/batch/delete`,
      method: 'post',
      data
    })
  }


//修改资源
export function updateAPI(data) {
  return request({
    url: `${requestPath.school}/schoolased-library/update`,
    method: 'post',
    data
  })
}

export function saveAPI(data) { //添加资源
    return request({
      url: `${requestPath.school}/schoolased-library/save`,
      method: 'post',
      data
    })
  }


export function detailsAPI(params) {//获取资源详情
    return request({
      url: `${requestPath.school}/schoolased-library/${params}`,
      method: 'get',
      params
    })
  }


  export function printAPI(data) { //导出校本库
    return request({
      url: `${requestPath.school}/schoolased-library/export`,
      method: 'get',
      params:data
    })
  }
  