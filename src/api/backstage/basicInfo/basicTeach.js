import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function getAcademicListAPI(data) {//学级列表
  return request({
    url: `${requestPath.school}/basic-teaching-plan/academic-level/list`,
    method: 'get',
    params: data
  })
}

export function getDepartmentListAPI(data) {//部门列表
  return request({
    url: `${requestPath.member}/department/down/all`,
    method: 'get',
    params: data
  })
}
export function getDepListAPI(data) {//部门筛选列表
  return request({
    url: `${requestPath.school}/basic-teaching-plan/department/list`,
    method: 'get',
    params: data
  })
}
export function getMajorListAPI(data) {//专业列表
    return request({
      url: `${requestPath.school}/major/page`,
      method: 'get',
      params: data
    })
  }
  export function getMajListAPI(data) {//专业筛选列表
    return request({
      url: `${requestPath.school}/basic-teaching-plan/major/list`,
      method: 'get',
      params: data
    })
  }
  






export function getListAPI(data) {//基础教学计划列表
    return request({
      url: `${requestPath.school}/basic-teaching-plan/page`,
      method: 'get',
      params: data
    })
  }
export function batchDeleteAPI(data) { //批量删除
    return request({
      url: `${requestPath.school}/basic-teaching-plan/batch/delete`,
      method: 'post',
      data
    })
  }



  
  export function getStudentResourceListAPI(data) {//生源列表
    return request({
      url: `${requestPath.school}/dictionarydata/list`,
      method: 'get',
      params: data
    })
  }
  export function getTrainListAPI(data) {//定向培养列表
    return request({
      url: `${requestPath.school}/dictionarydata/get/list`,
      method: 'get',
      params: data
    })
  }
//基础教学计划详情详情
export function getDetailAPI(params) {
    return request({
    url: `${requestPath.school}/basic-teaching-plan/${params}`,
    method: 'get',
    params
})
}

export function saveAPI(data) { //添加基础教学计划
    return request({
      url: `${requestPath.school}/basic-teaching-plan/save`,
      method: 'post',
      data
    })
  }
  export function getLastDetAPI(params) {//获取上一次添加详情
    return request({
      url: `${requestPath.school}/basic-teaching-plan/get`,
      method: 'get',
      params
    })
  }
  export function updateAPI(data) { //修改基础教学计划
    return request({
      url: `${requestPath.school}/basic-teaching-plan/update`,
      method: 'post',
      data
    })
  }






  export function getLinkListAPI(data) {//关联基础教学计划列表
    return request({
      url: `${requestPath.school}/pulic-lesson/page`,
      method: 'get',
      params: data
    })
  }
export function batchLinkDeleteAPI(data) { //关联批量删除
    return request({
      url: `${requestPath.school}/pulic-lesson/batch/delete`,
      method: 'post',
      data
    })
  }
  //基础教学计划关联详情
export function getDetailLinkAPI(params) {
  return request({
  url: `${requestPath.school}/pulic-lesson/${params}`,
  method: 'get',
  params
})
}

export function saveLinkAPI(data) { //添加关联基础教学计划
  return request({
    url: `${requestPath.school}/pulic-lesson/save/basic`,
    method: 'post',
    data
  })
}

export function updateLinkAPI(data) { //修改关联基础教学计划
  return request({
    url: `${requestPath.school}/pulic-lesson/update/basic`,
    method: 'post',
    data
  })
}
export function excelImportAPI(data) { //导入基础教学计划
  return request({
    url: `${requestPath.school}/basic-teaching-plan/import`,
    method: 'post',
    data
  })
}

export function excelImportLessonAPI(data,planId,type) { //导入基础教学计划课程
  return request({
    url: `${requestPath.school}/pulic-lesson/import?planId=${planId}&type=${type}`,
    method: 'post',
    data
  })
}