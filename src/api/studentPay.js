import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

//订单详情
export function getOrderDetailAPI(data){
    return request({
        url:`${requestPath.school}/student/order/info/pay`,
        method:'get',
        params:data
    })
}

// 确认支付
export function submitPayAPI(data){
    return request({
        url:`${requestPath.school}/student/order/confirm/pay`,
        method:'post',
        data
    })
}


