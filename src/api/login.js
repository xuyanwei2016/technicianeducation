import request from '@/utils/request'
import {requestPath} from '@/utils/global.js'
import md5 from 'blueimp-md5'

export function login(username, password, captcha, uuid) {


  const data = {
    name: username,
    password: md5(password),
    code: captcha,
    uuid: uuid
  }

  return request({
    url: `${requestPath.member}/permissions/login`,
    method: 'get',
    params: data
  })
}

export function getCapAPI(params) {
  return request({
    url: `${requestPath.member}/permissions/code?rand=${params}`,
    method: 'get'
  })
}

/*用户登录*/
export function loginAPI(data) {
  return request({
    url: `${requestPath.member}/permissions/login`,
    method: 'post',
    data

  })
}



export function getRouterList() {
  return request({
      url: `${requestPath.member}/user/permissions`,
      method: 'get'
  })
}

export function logout() {
  return request({
      url: '/login/logout',
      method: 'post'
  })
}


export function getPageButtonList(pageCode) {
const data = {
  mpCode: pageCode
}
return request({
  url: `${requestPath.member}/user/op`,
  method: 'get',
  params: data
})
}
