import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'


export function getListAPI(params) {//获取订单列表
    return request({
      url: `${requestPath.school}/order/back/page?pageParam.pageNum=${params.pageParam.pageNum}&pageParam.pageSize=${params.pageParam.pageSize}&isNot=${params.isNot}&memberType=${params.memberType}&supplierId=${params.supplierId}&opsType=${params.opsType}`,
      method: 'get',
    })
  }


export function getDisListAPI(data) {//获取供应商列表
    return request({
      url: `${requestPath.school}/supplier/page`,
      method: 'get',
      params: data
    })
  }


//修改订单状态
export function updateAPI(data) {
  return request({
    url: `${requestPath.school}/order/update/status`,
    method: 'post',
    data
  })
}

export function getDetailAPI(params) {//查看订单详情
  return request({
    url: `${requestPath.school}/orderinfo/back/page?pageParam.pageNum=${params.pageParam.pageNum}&pageParam.pageSize=${params.pageParam.pageSize}&orderId=${params.orderId}`,
    method: 'get',
  })
}

export function getOtherListAPI(params) {//其他学期学级列表
  return request({
    url: `${requestPath.school}/order/academic-level/all`,
    method: 'get',
    params
  })
}
  
export function excelImportAPI(data) { //导入退订订单
  return request({
    url: `${requestPath.school}/order/import`,
    method: 'post',
    data
  })
}