import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

//教材审核列表
export function getBookRecomListAPI(data){
    return request({
        url:`${requestPath.school}/book-recommend/mid/list`,
        method:'get',
        params:data
    })
}
// 其他学期列表
export function getOtherBookRecomListAPI(data){
    return request({
        url:`${requestPath.school}/book-recommend/page/history`,
        method:'get',
        params:data
    })
}
//前台推荐学级列表
export function getAcademicListAPI(params){
    return request({
        url:`${requestPath.school}/book/academic-level/list?status=${params}`,
        method:'get',
    })
}
//前台推荐专业列表
export function getMajorListAPI(params){
    return request({
        url:`${requestPath.school}/book/major/list?status=${params}`,
        method:'get',
    })
}
//前台终审专业列表
export function getMajListAPI(params){
    return request({
        url:`${requestPath.school}/book/major/list`,
        method:'get',
        params
    })
}
// 前台终审部门列表
export function getDeptListAPI(params){
    return request({
        url:`${requestPath.school}/book/department/list?status=${params}`,
        method:'get',
    })
}


//前台推荐培养层次列表
export function getTrainListAPI(params){
    return request({
        url:`${requestPath.school}/book/plan/list`,
        method:'get',
        params
    })
}
// 查看是否绑定教参习题册
export function getLessonDetailAPI(params){
    return request({
        url:`${requestPath.school}/book-recommend/mid/check/${params}`,
        method:'get',
        
    })
}

// 提交教材
export function submitBookAPI(data){
    return request({
        url:`${requestPath.school}/book-recommend/mid/submit`,
        method:'post',
        data
    })
}
// 退回教材

export function returnBookAPI(data){
    return request({
        url:`${requestPath.school}/book-recommend/mid/overrule`,
        method:'post',
        data
    })
}

// 终极审核提交
export function submitLastBookAPI(data){
    return request({
        url:`${requestPath.school}/book-recommend/last/submit`,
        method:'get',
        data
    })
}


// 获取推荐人列表
export function getRecommendListAPI(data){
    return request({
        url:`${requestPath.school}/book-recommend/referrer/list`,
        method:'post',
        data
    })
}
// 两个征订流程的确认

export function submitConfirmAPI(data){
    return request({
        url:`${requestPath.school}/book-recommend/confirm`,
        method:'post',
        data
    })
}
// 其他学期、
export function getOtherListAPI(params){
    return request({
        url:`${requestPath.school}/book/semester/list?academicLevel=${params}`,
        method:'get',
        
    })
}

export function getSubmitListAPI(params){
    return request({
        url:`${requestPath.school}/book-recommend/unsubmit?type=${params}`,
        method:'get',
        
    })
}
