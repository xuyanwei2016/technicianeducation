import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

//教材推荐环节列表
export function getBookRecomListAPI(data){
    return request({
        url:`${requestPath.school}/book-recommend/page`,
        method:'get',
        params:data
    })
}
// 其他学期列表
export function getOtherBookRecomListAPI(data){
    return request({
        url:`${requestPath.school}/book-recommend/page/history`,
        method:'get',
        params:data
    })
}
//前台推荐学级列表
export function getAcademicListAPI(params){
    return request({
        url:`${requestPath.school}/book/academic-level/list?status=${params}`,
        method:'get',
    })
}
//前台推荐专业列表
export function getMajorListAPI(params){
    return request({
        url:`${requestPath.school}/book/major/list?status=${params}`,
        method:'get',
    })
}
//前台推荐培养层次列表
export function getTrainListAPI(params){
    return request({
        url:`${requestPath.school}/book/plan/list`,
        method:'get',
        params
    })
}

// 其他学期、
export function getOtherListAPI(params){
    return request({
        url:`${requestPath.school}/book/semester/list?academicLevel=${params}`,
        method:'get',
        
    })
}
// 获取推荐人列表
export function getRecommendListAPI(data){
    return request({
        url:`${requestPath.school}/book-recommend/referrer/list`,
        method:'post',
        data
    })
}