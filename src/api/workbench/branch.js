import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

//教材审核列表
export function getListAPI(data){
    return request({
        url:`${requestPath.school}/split-order/page`,
        method:'get',
        params:data
    })
}
//前台推荐学级列表
export function getAcademicListAPI(data){
    return request({
        url:`${requestPath.school}/split-order/academicLevel/list`,
        method:'get',
        params:data
    })
}
//前台推荐部门列表
export function getDeptListAPI(data){
    return request({
        url:`${requestPath.school}/split-order/dept/list`,
        method:'get',
        params:data
    })
}
//出版社列表
export function getPressListAPI(data){
    return request({
        url:`${requestPath.school}/split-order/publisher/list`,
        method:'get',
        params:data
    })
}
//培养层次列表
export function getTrainListAPI(data){
    return request({
        url:`${requestPath.school}/split-order/plan/list`,
        method:'get',
        params:data
    })
}

//经销商列表
export function getSupplierListAPI(data){
    return request({
        url:`${requestPath.school}/split-order/supplier/list`,
        method:'get',
        params:data
    })
}



// 提交分单
export function submitUpAPI(data){
    return request({
        url:`${requestPath.school}/split-order/submit`,
        method:'post',
        data
    })
}


// 补订
export function submitMakeUpAPI(data){
    return request({
        url:`${requestPath.school}/order/save/make-up`,
        method:'post',
        data
    })
}

// 补订订单列表
export function getMakeUpListAPI(data){
    return request({
        url:`${requestPath.school}/orderinfo/make-up/book/list`,
        method:'get',
        params:data
    })
}
// 补订订单教材列表
export function getMakeUpBookListAPI(data){
    return request({
        url:`${requestPath.school}/orderinfo/make-up/guidebook/list`,
        method:'get',
        params:data
    })
}

//补订学级列表
export function getAcademicAPI(data){
    return request({
        url:`${requestPath.school}/orderinfo/academic-level/list`,
        method:'get',
        params:data
    })
}
//补订部门列表
export function getDeptAPI(data){
    return request({
        url:`${requestPath.school}/orderinfo/department/list`,
        method:'get',
        params:data
    })
}
//补订出版社列表
export function getPressAPI(data){
    return request({
        url:`${requestPath.school}/orderinfo/supplier/list`,
        method:'get',
        params:data
    })
}
//补订培养层次列表
export function getTrainAPI(data){
    return request({
        url:`${requestPath.school}/orderinfo/bpt/list`,
        method:'get',
        params:data
    })
}
// 修改订单数量
export function editOrderAPI(data){
    return request({
        url:`${requestPath.school}/split-order/edit`,
        method:'post',
        data
    })
}
// 其他学期、
export function getOtherListAPI(params){
    return request({
        url:`${requestPath.school}/split-order/semester/list?academicLevel=${params}`,
        method:'get',
        
    })
}
