import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'


export function getListAPI(data) {//获取订单列表
  return request({
    url: `${requestPath.school}/order/page`,
    method: 'get',
    params: data
  })
}


export function getDisListAPI(data) {//获取供应商列表
    return request({
      url: `${requestPath.school}/supplier/page`,
      method: 'get',
      params: data
    })
  }
  export function getDisSlectListAPI(data) {//列表页经销商列表
    return request({
      url: `${requestPath.school}/order/all`,
      method: 'post',
      data
    })
  }


//修改订单状态
export function updateAPI(data) {
  return request({
    url: `${requestPath.school}/order/update/status`,
    method: 'post',
    data
  })
}

export function getDetailAPI(params) {//查看订单详情
  return request({
    url: `${requestPath.school}/orderinfo/page`,
    method: 'get',
    params
  })
}

export function getOtherListAPI(params) {//其他学期学级列表
  return request({
    url: `${requestPath.school}/order/academic-level/all`,
    method: 'get',
    params
  })
}
  