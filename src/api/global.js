import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'
// import md5 from 'blueimp-md5'
//页脚友情链接
export function getFriendLinkAPI(){
    return request({
        url:`${requestPath.common}/links/list`,
        method:'get'
    })
}
// 页脚
export function getFooterListAPI(){
    return request({
        url:`${requestPath.common}/footer/list`,
        method:'get'
    })
}
// 修改点击量
export function changeClickNumAPI(data){
    return request({
        url:`${requestPath.common}/links/update/{oid}`,
        method:'post',
        data
    })
}

// 底部版权信息
export function getFooterCopyrightAPI(){
    return request({
        url:`${requestPath.common}/dictionary/get/footer`,
        method:'get'
    })
}

// 未读消息数量
export function getMessageNumAPI(){
    return request({
        url:`${requestPath.member}/member/count`,
        method:'get'
    })
}
// 获取登录用户单位信息
export function getUserCompanytAPI(){
    return request({
        url:`${requestPath.member}/member/member-company`,
        method:'get'
    })
}
// 用户属于征订流程的权限

export function getUserFlowAPI(){
    return request({
        url:`${requestPath.school}/bidding-flow/get/flow-code`,
        method:'get'
    })
}

// 获取当前征订季
export function getBiddingCycleAPI(){
  return request({
    url:`${requestPath.school}/bidding-cycle/get/bidding-cycle`,
    method:'get'
  })
}

