export  function changeContent(arr){
    arr.forEach(item => {
        item.content = item.content.replace(/<\/?.+?>/g,"").replace(/ /g,"").replace(/&\/?.+?;/g,"")
    });
    return arr
}

export  function changeValue(str){
    return str.replace(/<\/?.+?>/g,"").replace(/ /g,"")
}


