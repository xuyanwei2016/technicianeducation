import Cookies from 'js-cookie'

const TokenKey = 'Y-Token'


export function getToken() {//学校用户token
  return Cookies.get(TokenKey)
}
export function getSToken() {//学生token
  return Cookies.get('S-Token')
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function getUserType(){
  return Cookies.get('userType')//学校用户school，省监管用户supervise
}
