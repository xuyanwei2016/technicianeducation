// 报名和反馈更改数据
export function changeFromData(list,form){
  let data = JSON.parse(JSON.stringify(form))
      for(var prop in data){
        let obj ={}
        list.forEach(item=>{
          if(prop == item.name){
            let value = data[prop]
              obj = {
                name:item.name,
                type:item.type,
                value:value,
                options:item.value
              }
          }
          return obj
        })
        data[prop] = obj
      }
      return data
}

// 返显表单数据
export function changeFromList(list){
  let l =[]
  for(var prop in list){

    let obj = {
      name:list[prop].name,
      type:list[prop].type,
      value:list[prop].options
    }
    l.push(obj)

  }
  return l
}

// 判断活动中的所有字段是否都填完整
export function judgeValue(value,list){
  let status = list.length
  for(var prop in value){
    if(value[prop]){
      status -= 1
    }
    if(typeof(value[prop])=='object'&&value[prop].length==0){
      status += 1
    }
  }
  return status ===0
}
