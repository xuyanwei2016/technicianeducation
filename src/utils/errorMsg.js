import { Message, MessageBox } from 'element-ui'

export function errorMsg(data, msg) {
  data.code === -2 || data.code === -4 ? Message.error(`${msg}失败：${data.msg}`) : Message.error(`${msg}失败`)
}

/*
 ** 用法示例
 ** confirmBox('确认xxx吗？','批量操作')
 */
export function confirmBox(msg, title = '系统提示消息') {
  return MessageBox.confirm(msg, title, {
      confirmButtonText: '确定',
      cancelButtonText: '取消',
      type: 'warning'
      // center: true
  })
}