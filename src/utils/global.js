export const requestPath = {
    common: '/fg-sysuser/fg',
    member: '/fg-member/fg',
    resource: '/fg-resource/fg',
    file: '/file/file/',
    third: '/third/',
    school: '/school-sysuser/fg',
    bgmember: '/bg-member/bg',
    bgResource:'/bg-resource/bg'
}

export const uploadUrl = `${process.env.BASE_API}/file/file/`
export const uploadUrlT = `${process.env.BASE_API}/file`
export const baseUrl = `${process.env.BASE_API}`
export const downloadPath = 'download'
export const uploadPath = 'upload'
