// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'element-ui/lib/theme-chalk/index.css';
import '@/style/index.scss' // global css
import Vue from 'vue'
import App from './App'
import router from './router';
import LoginFooter from '@/components/loginFooter.vue';
import pagination from '@/components/pagination.vue';
import ComHeader from '@/components/header.vue';
import workBench from '@/view/workbench/components/index.vue';
import Cookies from 'js-cookie'
import store from './store';
import ElementUI from 'element-ui'
import {uploadUrlT} from '@/utils/global.js';
import Message from '@/components/message';
import VeeValidate from 'vee-validate';
import './permission' // permission control
import * as filters from './filters';
import "babel-polyfill";



const config = {
  errorBagName: 'errorBags', // change if property conflicts.
  fieldsBagName: 'fieldBags',
};
Vue.use(VeeValidate, config);
import vAlert from '@/components/alert.vue'; //全局组件
Vue.component('v-alert',vAlert)//全局组件
Vue.prototype.$my_message = Message.install;
Vue.config.productionTip = false;
Vue.component('LoginFooter', LoginFooter);
Vue.component('ComHeader', ComHeader);
Vue.component('workBench', workBench);
Vue.component('pagination', pagination);
Vue.use(ElementUI);
Vue.use(Cookies);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})


Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})


Vue.prototype.msg = function(msg) {

  this.$store.commit("tipsFun",msg)
  return
}
/*过滤空格*/
Vue.prototype.filterSpace = function(msg) {
  var result=msg.replace(/(^\s+)|(\s+$)/g,"");
  return result
}
/*过滤时间*/
Vue.prototype.filterTime = function(str) {

  if(str){
    let result=str.match(/[1-9][0-9]*/g);
    let day=result.length>2?(result[2]>9?'-'+result[2]:'-0'+result[2]):'';
    let timeStr=result[0]+'-'+(result[1]>9?result[1]:'0'+result[1])+day;
    return timeStr
  }

}

/*省略号*/
Vue.prototype.filterWord = function(str,num) {
  if(str){
    var msg = str.replace(/<\/?[^>]*>/g, '').replace(/&nbsp;/ig,''); //去除HTML Tag
    if(msg.length>num){
      var result=msg.slice(0,num)+'...';
      return result
    }else{
      return msg
    }
  }else{
    return ''
  }
}
/*图片路径*/
Vue.prototype.imgUrl = function(link) {
  return `${uploadUrlT}/` + link

}
/*图片类型*/
Vue.prototype.imgType = function(type) {
  if(type.indexOf('2-') == '-1'){
    return true
  }else{
    return false
  }
}
/*价格*/
Vue.prototype.filterPrice = function(link) {
  let str=link+'';
  let bb='';
  if(!link){
    bb='0.00'
  }else{
    if(str.indexOf('.')>=0){
      bb=link.toFixed(2)
    }else{
      bb=str+'.00'
    }
  }
  return bb
}
/*error图*/
Vue.prototype.errorImg = function(img,boo) {
  let imgHref=null;
  switch (img.type||img){
    case '1-1':
    case '1-2':
    case '1-3':
    case '1-4':
    case '1-5':
      imgHref='this.src="' + require('../static/img/imgHeight.png') + '"';
      break;
    case '2-2':
      imgHref='this.src="' + require('../static/img/ppt.png') + '"';
      break;
    case '2-4':
      imgHref='this.src="' + require('../static/img/audio.png') + '"';
      break;

    case 'Excel':
      imgHref='this.src="' + require('../static/img/Excel.png') + '"';
      break;
    case '2-1':
      imgHref='this.src="' + require('../static/img/PDF.png') + '"';
      break;
    case '2-3':
    case '2-7':
    case '2-9':
    case '2-11':
      imgHref='this.src="' + require(!boo?'../static/img/videoicon.png':'../static/img/video2.png') + '"';
      break;
    case '2-8':
      imgHref='this.src="' + require('../static/img/courseware.png') + '"';
      break;
    case '2-10':
    case '2-12':
      imgHref='this.src="' + require('../static/img/enclosure.png') + '"';
      break;
    case '2-5':
      imgHref='this.src="' + require('../static/img/imgWidth.png') + '"';
      break;
    case '2-6':

      let urlText=img.fileUrl?img.fileUrl:img.cover?img.cover:'null';
      if(urlText.slice(urlText.length-4,urlText.length).indexOf('doc'||'docx')>=0){
        imgHref='this.src="' + require('../static/img/word.png') + '"';
      }else if(urlText.slice(urlText.length-4,urlText.length).indexOf('txt')>=0){
        imgHref='this.src="' + require('../static/img/text.png') + '"';
      }else if(urlText.slice(urlText.length-4,urlText.length).indexOf('ppt'||'pptx')>=0){
        imgHref='this.src="' + require('../static/img/ppt.png') + '"';

      }else{
        imgHref='this.src="' + require('../static/img/word.png') + '"';
      }

      break;

  }
  return imgHref
}
/*图片 文本*/
Vue.prototype.filterTextImg = function(img) {
  let imgHref=null;
  if(img.type=='2-6'){
    let urlText=img.fileUrl?img.fileUrl:img.cover?img.cover:'null';
    if(urlText.slice(urlText.length-4,urlText.length).toLowerCase().indexOf('doc'||'docx')>=0){
      imgHref='./static/img/word1.png';
    }else if(urlText.slice(urlText.length-4,urlText.length).toLowerCase().indexOf('txt')>=0){
      imgHref='./static/img/txt1.png';
    }else if(urlText.slice(urlText.length-4,urlText.length).toLowerCase().indexOf('ppt'||'pptx')>=0){
      imgHref='./static/img/ppt1.png';

    }else if(urlText.slice(urlText.length-4,urlText.length).toLowerCase().indexOf('zip')>=0){
      imgHref='./static/img/zip1.png';

    }else{
      imgHref='./static/img/word1.png';
    }
  }else if(img.type=='2-9'){
    if(img.cover){
      imgHref=this.imgUrl(img.cover);
    }else{
      imgHref='./static/img/video2.png';
    }

  }else{
    if(img.cover){
      imgHref=this.imgUrl(img.cover);
    }else{
      imgHref='./static/img/enclosure1.png';
    }
  }

  return imgHref
}
Vue.prototype.fileImg = function(img) {
  let imgHref=null;
  if(img.cover){
    imgHref=this.imgUrl(img.cover);
  }else {
    let len = img.fileUrl.length - 4;
    let fileType = img.fileUrl.substring(len).toLowerCase();
    switch (fileType) {
      case '.ppt':
      case '.pps':
      case 'pptx':
        imgHref = './static/img/ppt2.png';
        break;
      case '.jpg':
      case 'jpeg':
      case '.gif':
      case '.png':
        imgHref = this.imgUrl(img.fileUrl);
        break;
      case '.mp4':
      case '.avi':
      case '.mov':
      case '.flv':
      case '.swf':
      case 'rmvb':
      case '.fla':
        imgHref = './static/img/video2.png';
        break;
      case 'xlsx':
      case '.xls':
        imgHref = './static/img/Excel1.png';
        break;
      case '.pdf':
      imgHref = './static/img/PDF2.png';
      break;
      case '.mp3':
      case '.wma':
      case '.wav':
        imgHref = './static/img/audio2.png';
        break;
      case '.txt':
        imgHref = './static/img/TXT2.png';
        break;
      case '.doc':
      case 'docx':
        imgHref = './static/img/word2.png';
        break;
      case '.rar':
        imgHref = './static/img/RAR2.png';
        break;
      case '.zip':
        imgHref = './static/img/zip2.png';
        break;
      case '.htm':
      case 'html':
        imgHref = './static/img/courseware2.png';
        break;
        default:
            imgHref = './static/img/imgWidth.png';
          break;
    }

  }
  return imgHref;
}

/*图片*/
Vue.prototype.filterImg = function(img) {
  let imgHref=null;
  if(img.cover){
    imgHref=this.imgUrl(img.cover);
  }else{
    switch (img.type){
      case '.ppt':
      case 'pptx':
        imgHref='./static/img/ppt2.png';
        break;
      case '2-4':
        imgHref='./static/img/audio2.png';
        break;
      case '1-1':
      case '1-2':
      case '1-3':
      case '1-4':
      case '1-5':
        imgHref='./static/img/imgHeight.png';
        break;
      case 'Excel':
        imgHref='./static/img/Excel1.png';
        break;
      case '2-1':
        imgHref='./static/img/PDF2.png';
        break;
      case '2-3':
      case '2-7':
      case '2-9':
      case '2-11':
        imgHref='./static/img/video2.png';
        break;
      case '2-8':
        imgHref='./static/img/courseware2.png';
        break;
      case '2-10':
      case '2-12':
        imgHref='./static/img/enclosure2.png';
        break;
      case '2-5':
        imgHref='./static/img/imgWidth.png';
        break;
      case '2-2':
        imgHref='./static/img/ppt2.png';
        break;
      case '2-6':

        let urlText=img.fileUrl?img.fileUrl:img.cover?img.cover:'null';

        let urlTextslice=urlText.slice(urlText.length-4,urlText.length).toLowerCase();

        if(urlTextslice.indexOf('doc')>=0){
          imgHref='./static/img/word2.png';
        }else if(urlTextslice.indexOf('txt')>=0){
          imgHref='./static/img/TXT2.png';
        }else if(urlTextslice.indexOf('ppt')>=0){
          imgHref='./static/img/ppt2.png';

        }else if(urlTextslice.indexOf('xls')>=0){
          imgHref='./static/img/Excel1.png';

        }else{
          imgHref='./static/img/word2.png';
        }

        break;

    }
  }

  return imgHref
}

/*两行省略*/
Vue.prototype.subWord = function(str){
  var templen=0;
  for(var i=0;i<str.length;i++){
    if(str.charCodeAt(i)>127){
      templen+=2;
    }else{
      templen++
    }
    if(templen == 68){
      if(str.charCodeAt(i)==32){
        return str.substring(0,i)+'...';
      }else{
        return str.substring(0,i+1)+'...';
      }
    }else if(templen >68){
      if(str.charCodeAt(i)==32){

        return str.substring(0,i-1)+'...';
      }else{
        return str.substring(0,i)+'...';
      }

    }
  }

  return str;
},




  Vue.prototype.Ellipsis = function(id, rows, str,lineH) {
    str = str.replace(/<\/?[^>]*>/g, '').replace(/&nbsp;/ig,'');
  if (!str || str == '') {
    return ''
  }
  this.$nextTick(function () {
    var text = document.getElementById(id);
    var lineHeight = lineH;
    var at = rows * parseInt(lineHeight);
    var tempstr = str;
    text.innerHTML = tempstr;
    var len = tempstr.length;
    var i = 0;
    if (text.offsetHeight <= at) {
    }else {
      var temp = "";
      text.innerHTML = temp;
      while (text.offsetHeight <= at) {
        temp = tempstr.substring(0, i + 1);
        i++;
        text.innerHTML = temp;
      }
      var slen = temp.length;
      tempstr = temp.substring(0, slen - 1);
      len = tempstr.length
      text.innerHTML = tempstr.substring(0, len - 3) + "...";
      text.height = at + "px";
    }
    return str
  })


}

// 过滤图片地址缺少/
Vue.prototype.filterImgAddress = function(src) {
  if(src.substr(0,1) == '/'){
    return src
  }else{
    return src ='/'+src
  }
}
// Vue.prototype.showImg = function(img){
//   return `${uploadUrlT}` + img.cover
// }
Vue.prototype.imgError = function(img) {
    img.cover = '/img/01-图书.png'
}
// 列表高度
Vue.prototype.tableHeigh = function (height) {
  return (window.innerHeight||document.documentElement.clientHeight||document.body.clientHeight)-height+'px'
}

// router.beforeEach((to, from, next) => {

//   if (Cookies.get('Y-Token')) {
//     if (to.path === '/login') {
//       next({path: '/index'})
//     } else if (to.path === '/personal') {
//       next({path: '/personal/detail'})
//     }else {
//       next()
//     }
//   } else {

//     if (to.path.indexOf('personal') >= 0||to.path.indexOf('backstage') >= 0||to.path.indexOf('jurisdiction') >= 0||to.path.indexOf('sysMenege') >= 0) {
//       if(to.path === '/login'){
//         store.commit('historyRouter',from.fullPath);
//       }
//       next({path: '/login'})
//     }else{
//       if(to.path === '/login'){
//         store.commit('historyRouter',from.fullPath);
//       }
//       next()
//     }


//   }

// });

