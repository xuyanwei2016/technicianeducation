import Vue from 'vue'
import Router from 'vue-router';
import forgetIndex from "@/view/login/forget.vue";
// const _import = require('./_import_' + process.env.NODE_ENV)
import Layout from '../view/backstage/index.vue'

Vue.use(Router)

export const constantRouterMap = [
  {
// export const constantRouterMap = [{


  path: '/',
  redirect: '/index',
  component: () => import('@/view/home/main.vue'),
  hidden:true,
  children: [
    // {
    //   path: 'index',
    //   name: 'Index',
    //   meta: {title: '首页'},
    //   component: () => import('@/view/home/index.vue'),
    //   hidden: true
    // },
    {
      path: '/',
      name: 'Main',
      meta: {title: '主页'},
      redirect: '/index',
      component: () => import('@/view/home/main.vue'),
      hidden: true,
      children: [
        {
          path: 'index',
          name: 'Index',
          meta: {title: '首页'},
          component: () => import('@/view/home/index.vue'),
        },
        // 省监管首页
        {
          path: 'supervise',
          name: 'supervise',
          meta: {title: '省监管首页'},
          component: () => import('@/view/home/supervise.vue'),
        },
        
        // 学生支付
        {
          path: 'studentPay',
          name: 'studentPay',
          meta: {title: '学生支付'},
          component: () => import('@/view/studentPay/index.vue'),
          hidden: true
        },
        // 资源
        {
          path: 'searchDefault',
          name: 'searchDefault',
          meta: {title: '资源检索'},
          component: () => import('@/view/resources/searchDefault.vue'),
        },
        // 资源检索
        {
          path: 'materialType',
          name: 'materialType',
          meta: {title: '资源检索'},
          component: () => import('@/view/resources/materialType.vue'),
        },

        {
          path: 'promote',
          name: 'promote',
          meta: {title: '职业能力提升'},
          component: () => import('@/view/resources/promote.vue'),
        },
        {
          path: 'text',
          name: 'text',
          meta: {title: '教育教材'},
          component: () => import('@/view/bookDetail/text.vue'),
        },
        {
          path: 'resources',
          name: 'resources',
          redirect: '/materialType',
          meta: {title: '资源检索'},
          component: () => import('@/view/bookDetail/index.vue'),
          children: [{
            path: 'info',
            name: 'info',
            meta: {title: '教育教材'},
            component: () => import('@/view/bookDetail/info.vue'),
          },
            {
              path: 'vocation',
              name: 'vocation',
              meta: {title: '职业培训'},
              component: () => import('@/view/bookDetail/vocation.vue'),
            },
            {
              path: 'teaching',
              name: 'teaching',
              meta: {title: '教辅资源'},
              component: () => import('@/view/bookDetail/teaching.vue'),
            }
          ]
        },
        // 省监管列表
        {
          path: 'regulatoryIndex',
          name: 'regulatoryIndex',
          redirect: 'regulatoryIndex/regulatoryList',
          meta: {title: '监管'},
          component: () => import('@/view/regulatory/index.vue'),
          children: [
            {
              path: 'regulatoryList',
              name: 'regulatoryList',
              meta: {title: '监管列表'},
              component: () => import('@/view/regulatory/regulatoryList.vue'),
            },
            {
            path: 'regulatoryDetail',
            name: 'regulatoryDetail',
            meta: {title: '教材列表'},
            component: () => import('@/view/regulatory/regulatoryDetail.vue'),
          },
          ]
        },

        // 培训机构列表
        {
          path: 'trainIndex',
          name: 'trainIndex',
          redirect: 'trainIndex/trainList',
          meta: {title: '机构'},
          component: () => import('@/view/train/index.vue'),
          children: [
            {
              path: 'trainList',
              name: 'trainList',
              meta: {title: '培训机构'},
              component: () => import('@/view/train/trainList.vue'),
            },
            {
            path: 'trainDetail',
            name: 'trainDetail',
            meta: {title: '培训机构教材列表'},
            component: () => import('@/view/train/trainDetail.vue'),
          },
          ]
        },
        // 数据统计
        {
          path: 'dataIndex',
          name: 'dataIndex',
          redirect: 'dataIndex/dataList',
          meta: {title: '数据'},
          component: () => import('@/view/dataStatistics/index.vue'),
          children: [
            {
              path: 'dataList',
              name: 'dataList',
              meta: {title: '数据统计'},
              component: () => import('@/view/dataStatistics/dataList.vue'),
            },
            {
            path: 'dataDetail',
            name: 'dataDetail',
            meta: {title: '详情'},
            component: () => import('@/view/dataStatistics/dataDetail.vue'),
          },
          ]
        },

        {
          path: 'noticeList',
          name: 'noticeList',
          meta: {title: '公告通知'},
          component: () => import('@/view/notice/noticeList.vue'),
        },
        {
          path: 'noticeIndex',
          name: 'noticeIndex',
          redirect: '/noticeList',
          meta: {title: '公告通知'},
          component: () => import('@/view/notice/index.vue'),
          children: [{
            path: 'noticeDetail',
            name: 'noticeDetail',
            meta: {title: '公告详情'},
            component: () => import('@/view/notice/noticeDetail.vue'),
          },
          ]
        },
        {
          path: 'mechanicList',
          name: 'mechanicList',
          meta: {title: '技工动态'},
          component: () => import('@/view/forum/forumList.vue'),
        },
        {
          path: 'mechanicIndex',
          name: 'mechanicIndex',
          redirect: '/mechanicList',
          meta: {title: '技工动态'},
          component: () => import('@/view/forum/index.vue'),
          children: [{
            path: 'mechanicDetail',
            name: 'mechanicDetail',
            meta: {title: '动态详情'},
            component: () => import('@/view/forum/forumDetail.vue'),
          },
          ]
        },
        {
          path: 'stationList',
          name: 'stationList',
          meta: {title: '站内信息',meta:{keepAlive:true}},
          component: () => import('@/view/notice/noticeList.vue'),
        },
        {
          path: 'stationIndex',
          name: 'stationIndex',
          redirect: '/stationList',
          meta: {title: '站内消息'},
          component: () => import('@/view/notice/index.vue'),
          children: [{
            path: 'stationDetail',
            name: 'stationDetail',
            meta: {title: '查看消息'},
            component: () => import('@/view/notice/noticeDetail.vue'),
          },
          ]
        },
        {
          path: 'unionList',
          name: 'unionList',
          meta: {title: '联盟之声'},
          component: () => import('@/view/forum/forumList.vue'),
        },
        {
          path: 'unionIndex',
          name: 'unionIndex',
          redirect: '/unionList',
          meta: {title: '联盟之声'},
          component: () => import('@/view/forum/index.vue'),
          children: [{
            path: 'unionDetail',
            name: 'unionDetail',
            meta: {title: '联盟之声详情'},
            component: () => import('@/view/forum/forumDetail.vue'),
          },
          ]
        },
        {
          path: 'forumList',
          name: 'forumList',
          meta: {title: '论坛之音'},
          component: () => import('@/view/forum/forumList.vue'),
        },
        {
          path: 'forumIndex',
          name: 'forumIndex',
          redirect: '/forumList',
          meta: {title: '论坛之音'},
          component: () => import('@/view/forum/index.vue'),
          children: [{
            path: 'forumDetail',
            name: 'forumDetail',
            meta: {title: '论坛之音详情'},
            component: () => import('@/view/forum/forumDetail.vue'),
          },
          ]
        },
        {
          path: 'footerDetail',
          name: 'footerDetail',
          meta: {title: '关于我们'},
          component: () => import('@/view/footer/index.vue'),
        },
        //个人中心
        {
          path: 'personal',
          name: 'personal',
          redirect: '/personal/detail',
          meta: {title: '个人中心'},
          component: () => import('@/view/personal/index.vue'),
          children: [{
            path: 'detail',
            name:'detail',
            meta: {title: '个人中心'},
            component: () => import('@/view/personal/detail.vue'),
          },
            {
              path: 'book',
              name: 'book',
              meta:{title:'样书申请'},
              component: () => import('@/view/personal/bookApply/index.vue')
            }, {
              path: 'integral',
              name: 'integral',
              meta:{title:'我的积分'},
              component: () => import('@/view/personal/integral/index.vue')
            },
            {
              path: 'integralExchange',
              name: 'integralExchange',
              meta: {title: '兑换礼品或样书券'},
              component: () => import('@/view/personal/integral/integralPage.vue'),
            },{
              path: 'activity',
              name: 'activity',
              meta:{title:'活动报名'},
              component: () => import('@/view/personal/activity/index.vue')
            },
            {
              path: 'activityList',
              name:'activityList',
              meta: {title: '活动列表'},
              component: () => import('@/view/personal/activity/list.vue')
            },
            {
              path: 'activityDetail',
              name:'activityDetail',
              meta: {title: '活动详情'},
              component: () => import('@/view/personal/activity/activityDetail.vue')
            },
            {
              path: 'gift',
              name: 'gift',
              meta:{title:'我的礼品'},
              component: () => import('@/view/personal/gift/index.vue')
            }, {
              path: 'evaluate',
              name: 'evaluate',
              meta: {title: '我的评价'},
              component: () => import('@/view/personal/evaluate/index.vue')
            }, 
            {
              path: 'feedback',
              name: 'feedback',
              meta: {title: '我的反馈'},
              component: () => import('@/view/personal/feedback/index.vue'),
            },
            {
              // path: 'feedbackIndex',
              // name: 'feedbackIndex',
              // redirect: 'feedbackIndex/feedbackDetail',
              // meta: {title: '我的反馈'},
              // component: () => import('@/view/feedBack/index.vue'),
              // children: [
                // {
                path: 'feedBackDetail',
                name: 'feedBackDetail',
                meta: {title: '意见征集详情'},
                component: () => import('@/view/personal/feedback/feedBackDetail.vue'),
              },
              // ]
            // }
            {
              path: 'collection',
              name: 'collection',
              meta: {title: '我的收藏'},
              component: () => import('@/view/personal/collection/index.vue')
            }, {
              path: 'record',
              name: 'record',
              meta: {title: '浏览记录'},
              component: () => import('@/view/personal/record/index.vue')
            }, {
              path: 'infomess',
              name: 'infomess',
              meta: {title: '个人信息'},
              component: () => import('@/view/personal/info/index.vue')
            }, {
              path: 'account',
              name: 'account',
              meta: {title: '账户安全'},
              component: () => import('@/view/personal/account/index.vue')
            }, {
              path: 'train',
              name: 'train',
              meta: {title: '在线培训'},
              component: () => import('@/view/personal/train/index.vue')
            }
          ]
        },
        //工作台
        {
          path: 'workbench',
          name: 'workbench',
          redirect: '/workbench/order',
          meta: {title: '工作台'},
          component: () => import('@/view/workbench/index.vue'),
          children:[
            {
              path: 'recommend',
              name: 'recommend',
              meta: {title: '教材推荐'},
              component: () => import('@/view/workbench/recommend/index.vue'),
            },
            {
              path: 'examine',
              name: 'examine',
              meta: {title: '初审'},
              component: () => import('@/view/workbench/recommend/examine.vue'),
            },
            {
              path: 'middleExamine',
              name: 'middleExamine',
              meta: {title: '中审'},
              component: () => import('@/view/workbench/recommend/middleExamine.vue'),
            },
            {
              path: 'judgment',
              name: 'judgment',
              meta: {title: '终极审核'},
              component: () => import('@/view/workbench/recommend/judgment.vue'),
            },

            {
              path: 'branch',
              name: 'branch',
              meta: {title: '分单'},
              component: () => import('@/view/workbench/branch/index.vue'),
            },
            {
              path: 'order',
              name: 'order',
              meta: {title: '订单管理'},
              component: () => import('@/view/workbench/order/index.vue'),
            },
            {
              path: 'addOrder',
              name: 'addOrder',
              meta: {title: '添加订单'},
              component: () => import('@/view/workbench/order/addOrder.vue'),
            },
            {
              path: 'supplements',
              name: 'supplements',
              meta: {title: '补订记录'},
              component: () => import('@/view/workbench/supplements/index.vue'),
            },
            {
              path: 'unsubscribe',
              name: 'unsubscribe',
              meta: {title: '退订记录'},
              component: () => import('@/view/workbench/unsubscribe/index.vue'),
            },
            {
              path: 'trainBook',
              name: 'trainBook',
              meta: {title: '培训教材订购'},
              component: () => import('@/view/workbench/trainBook/index.vue'),
            },
            {
              path: 'addTrainBook',
              name: 'addTrainBook',
              meta: {title: '添加培训教材订单',
              routerIds: ['0407']},
              component: () => import('@/view/workbench/trainBook/addTrainBook.vue'),
            },
            {
              path: 'organizationOrder',
              name: 'organizationOrder',
              meta: {title: '机构教材订购',
              routerIds: ['0408']},
              component: () => import('@/view/workbench/organizationOrder/index.vue'),
            },
            {
              path: 'addOrganOrder',
              name: 'addOrganOrder',
              meta: {title: '添加机构教材订单',
              routerIds: ['0408']},
              component: () => import('@/view/workbench/organizationOrder/addOrganOrder.vue'),
            },

          ],
        },


    {
      path: '/login',
      name: 'login',
      meta: {title: '经销商登录'},
      component: () =>
        import ('@/view/login/index.vue'),
        hidden: true
    },

    {
      path: '/forgetIndex',
      name: 'forgetIndex',
      /*redirect:'/forumList',*/
      component: () => import ('@/view/login/forget.vue'),
      hidden: true,
      children: [{
        path: '/forgetPass',
        name: 'forgetPass',
        meta: {title: '忘记密码'},
        component: () => import ('@/view/login/forgetPass.vue')
      }, {
        path: '/forgetchange',
        name: 'forgetchange',
        meta: {title: '忘记密码选择方式'},
        component: () => import ('@/view/login/forgetchange.vue')
      }, {
        path: '/forgetchangeemail',
        name: 'forgetchangeemail',
        meta: {title: '忘记密码邮箱找回'},
        component: () => import ('@/view/login/forgetChangeEmail.vue')
      }
      ]
    },
    ]
    },
  ]
}
]


const createRouter = ()=>new Router({
  scrollBehavior: () => ({
    y: 0
  }),
  routes: constantRouterMap
})
const router = createRouter()
// new Router({

  // mode: 'history', // require service support
  // scrollBehavior: () => ({
  //   y: 0
  // }),
  // routes: constantRouterMap
// })



export function resetRouter(){
  const newRouter = createRouter();
  router.matcher = newRouter.matcher

}

export default router




export const asyncRouterMap = [
  {
  path: '/',
  redirect: '/index',
  component: () => import('@/view/home/main.vue'),
  hidden:true,
  children: [
    // {
    //   path: 'index',
    //   name: 'Index',
    //   meta: {title: '首页'},
    //   component: () => import('@/view/home/index.vue'),
    //   hidden: true,
    // },
    {
      path: '/',
      name: 'Main',
      meta: {title: '主页'},
      redirect: '/index',
      component: () => import('@/view/home/main.vue'),
      hidden: true,
      children: [
        {
          path: 'index',
          name: 'Index',
          meta: {title: '首页'},
          component: () => import('@/view/home/index.vue'),
        },
        {
          path: 'studentPay',
          name: 'studentPay',
          meta: {title: '学生支付'},
          component: () => import('@/view/studentPay/index.vue'),
          hidden: true
        },
        {
          path: 'searchDefault',
          name: 'searchDefault',
          meta: {title: '资源检索'},
          component: () => import('@/view/resources/searchDefault.vue'),
        },
        {
          path: 'materialType',
          name: 'materialType',
          meta: {title: '资源检索'},
          component: () => import('@/view/resources/materialType.vue'),
        },
        {
          path: 'promote',
          name: 'promote',
          meta: {title: '职业能力提升'},
          component: () => import('@/view/resources/promote.vue'),
        },{
          path: 'text',
          name: 'text',
          meta: {title: '教育教材'},
          component: () => import('@/view/bookDetail/text.vue'),
        },
        {
          path: 'resources',
          name: 'resources',
          redirect: '/materialType',
          meta: {title: '资源检索'},
          component: () => import('@/view/bookDetail/index.vue'),
          children: [{
            path: 'info',
            name: 'info',
            meta: {title: '教育教材'},
            component: () => import('@/view/bookDetail/info.vue'),
          },
            {
              path: 'vocation',
              name: 'vocation',
              meta: {title: '职业培训'},
              component: () => import('@/view/bookDetail/vocation.vue'),
            },
            {
              path: 'teaching',
              name: 'teaching',
              meta: {title: '教辅资源'},
              component: () => import('@/view/bookDetail/teaching.vue'),
            }
          ]
        },
        {
          path: 'noticeList',
          name: 'noticeList',
          meta: {title: '公告通知'},
          component: () => import('@/view/notice/noticeList.vue'),
        },
        {
          path: 'noticeIndex',
          name: 'noticeIndex',
          redirect: '/noticeList',
          meta: {title: '公告通知'},
          component: () => import('@/view/notice/index.vue'),
          children: [{
            path: 'noticeDetail',
            name: 'noticeDetail',
            meta: {title: '公告详情'},
            component: () => import('@/view/notice/noticeDetail.vue'),
          },
          ]
        },
        {
          path: 'mechanicList',
          name: 'mechanicList',
          meta: {title: '技工动态'},
          component: () => import('@/view/forum/forumList.vue'),
        },
        {
          path: 'mechanicIndex',
          name: 'mechanicIndex',
          redirect: '/mechanicList',
          meta: {title: '技工动态'},
          component: () => import('@/view/forum/index.vue'),
          children: [{
            path: 'mechanicDetail',
            name: 'mechanicDetail',
            meta: {title: '动态详情'},
            component: () => import('@/view/forum/forumDetail.vue'),
          },
          ]
        },
        {
          path: 'stationList',
          name: 'stationList',
          meta: {title: '站内信息',meta:{keepAlive:true}},
          component: () => import('@/view/notice/noticeList.vue'),
        },
        {
          path: 'stationIndex',
          name: 'stationIndex',
          redirect: '/stationList',
          meta: {title: '站内消息'},
          component: () => import('@/view/notice/index.vue'),
          children: [{
            path: 'stationDetail',
            name: 'stationDetail',
            meta: {title: '查看消息'},
            component: () => import('@/view/notice/noticeDetail.vue'),
          },
          ]
        },
        {
          path: 'unionList',
          name: 'unionList',
          meta: {title: '联盟之声'},
          component: () => import('@/view/forum/forumList.vue'),
        },
        {
          path: 'unionIndex',
          name: 'unionIndex',
          redirect: '/unionList',
          meta: {title: '联盟之声'},
          component: () => import('@/view/forum/index.vue'),
          children: [{
            path: 'unionDetail',
            name: 'unionDetail',
            meta: {title: '联盟之声详情'},
            component: () => import('@/view/forum/forumDetail.vue'),
          },
          ]
        },
        {
          path: 'forumList',
          name: 'forumList',
          meta: {title: '论坛之音'},
          component: () => import('@/view/forum/forumList.vue'),
        },
        {
          path: 'forumIndex',
          name: 'forumIndex',
          redirect: '/forumList',
          meta: {title: '论坛之音'},
          component: () => import('@/view/forum/index.vue'),
          children: [{
            path: 'forumDetail',
            name: 'forumDetail',
            meta: {title: '论坛之音详情'},
            component: () => import('@/view/forum/forumDetail.vue'),
          },
          ]
        },

        {
          path: 'footerDetail',
          name: 'footerDetail',
          meta: {title: '关于我们'},
          component: () => import('@/view/footer/index.vue'),
        },

        {
          path: 'personal',
          name: 'personal',
          redirect: '/personal/detail',
          meta: {title: '个人中心'},
          component: () => import('@/view/personal/index.vue'),
          children: [{
            path: 'detail',
            name:'detail',
            meta: {title: '个人中心'},
            component: () => import('@/view/personal/detail.vue'),
          },
            {
              path: 'book',
              name: 'book',
              meta:{title:'样书申请'},
              component: () => import('@/view/personal/bookApply/index.vue')
            }, {
              path: 'integral',
              name: 'integral',
              meta:{title:'我的积分'},
              component: () => import('@/view/personal/integral/index.vue')
            },
            {
              path: 'integralExchange',
              name: 'integralExchange',
              meta: {title: '兑换礼品或样书券'},
              component: () => import('@/view/personal/integral/integralPage.vue'),
            },
            // {
            //   path: 'activity',
            //   name: 'activity',
            //   meta:{title:'活动报名'},
            //   component: () => import('@/view/personal/activity/index.vue')
            // },
            // {
            //   path: 'activityList',
            //   name:'activityList',
            //   meta: {title: '活动列表'},
            //   component: () => import('@/view/personal/activity/list.vue')
            // },
            // {
            //   path: 'activityDetail',
            //   name:'activityDetail',
            //   meta: {title: '活动详情'},
            //   component: () => import('@/view/personal/activity/activityDetail.vue')
            // },
            {
              path: 'gift',
              name: 'gift',
              meta:{title:'我的礼品'},
              component: () => import('@/view/personal/gift/index.vue')
            }, {
              path: 'evaluate',
              name: 'evaluate',
              meta: {title: '我的评价'},
              component: () => import('@/view/personal/evaluate/index.vue')
            }, {
              path: 'feedback',
              name: 'feedback',
              meta: {title: '我的反馈'},
              component: () => import('@/view/personal/feedback/index.vue'),
            },
            {
              // path: 'feedbackIndex',
              // name: 'feedbackIndex',
              // redirect: 'feedbackIndex/feedbackDetail',
              // meta: {title: '我的反馈'},
              // component: () => import('@/view/feedBack/index.vue'),
              // children: [
                // {
                path: 'feedBackDetail',
                name: 'feedBackDetail',
                meta: {title: '意见征集详情'},
                component: () => import('@/view/personal/feedback/feedBackDetail.vue'),
              },
              // ]
            // }
            {
              path: 'collection',
              name: 'collection',
              meta: {title: '我的收藏'},
              component: () => import('@/view/personal/collection/index.vue')
            }, {
              path: 'record',
              name: 'record',
              meta: {title: '浏览记录'},
              component: () => import('@/view/personal/record/index.vue')
            }, {
              path: 'info',
              name: 'info',
              meta: {title: '个人信息'},
              component: () => import('@/view/personal/info/index.vue')
            }, {
              path: 'account',
              name: 'account',
              meta: {title: '账户安全'},
              component: () => import('@/view/personal/account/index.vue')
            }, {
              path: 'train',
              name: 'train',
              meta: {title: '在线培训'},
              component: () => import('@/view/personal/train/index.vue')
            }
          ]
        },
        //工作台
        {
          path: 'workbench',
          name: 'workbench',
          redirect: '/workbench/recommend',
          meta: {title: '工作台',
          routerIds: ['04']},
          component: () => import('@/view/workbench/index.vue'),
          children:[
            {
              path: 'recommend',
              name: 'recommend',
              meta: {title: '教材推荐'},
              component: () => import('@/view/workbench/recommend/index.vue'),
            },
            {
              path: 'examine',
              name: 'examine',
              meta: {title: '初审'},
              component: () => import('@/view/workbench/recommend/examine.vue'),
            },
            {
              path: 'middleExamine',
              name: 'middleExamine',
              meta: {title: '中审'},
              component: () => import('@/view/workbench/recommend/middleExamine.vue'),
            },
            {
              path: 'judgment',
              name: 'judgment',
              meta: {title: '终极审核'},
              component: () => import('@/view/workbench/recommend/judgment.vue'),
            },

            {
              path: 'branch',
              name: 'branch',
              meta: {title: '分单',
              routerIds: ['0402']},
              component: () => import('@/view/workbench/branch/index.vue'),
            },
            {
              path: 'order',
              name: 'order',
              meta: {title: '订单管理',
              routerIds: ['0403']},
              component: () => import('@/view/workbench/order/index.vue'),
            },
            {
              path: 'addOrder',
              name: 'addOrder',
              meta: {title: '添加订单',
              routerIds: ['0403']},
              component: () => import('@/view/workbench/order/addOrder.vue'),
            },
            {
              path: 'supplements',
              name: 'supplements',
              meta: {title: '补订记录',
              routerIds: ['0405']},
              component: () => import('@/view/workbench/supplements/index.vue'),
            },
            {
              path: 'unsubscribe',
              name: 'unsubscribe',
              meta: {title: '退订记录',
              routerIds: ['0406']},
              component: () => import('@/view/workbench/unsubscribe/index.vue'),
            },
            {
              path: 'trainBook',
              name: 'trainBook',
              meta: {title: '培训教材订购',
              routerIds: ['0407']},
              component: () => import('@/view/workbench/trainBook/index.vue'),
            },
            {
              path: 'addTrainBook',
              name: 'addTrainBook',
              meta: {title: '添加培训教材订单',
              routerIds: ['0407']},
              component: () => import('@/view/workbench/trainBook/addTrainBook.vue'),
            },
            {
              path: 'organizationOrder',
              name: 'organizationOrder',
              meta: {title: '机构教材订购',
              routerIds: ['0408']},
              component: () => import('@/view/workbench/organizationOrder/index.vue'),
            },
            {
              path: 'addOrganOrder',
              name: 'addOrganOrder',
              meta: {title: '添加机构教材订单',
              routerIds: ['0408']},
              component: () => import('@/view/workbench/organizationOrder/addOrganOrder.vue'),
            },

          ]
        },
      // ]
    // },

    // {
    //   path: '/login',
    //   name: 'login',
    //   meta: {title: '经销商登录'},
    //   component: () =>
    //     import ('@/view/login/index.vue'),
    //     hidden: true
    // },
    // {
    //   path: '/forgetIndex',
    //   name: 'forgetIndex',
    //   /*redirect:'/forumList',*/
    //   component: () => import ('@/view/login/forget.vue'),
    //   hidden: true,
    //   children: [{
    //     path: '/forgetPass',
    //     name: 'forgetPass',
    //     meta: {title: '忘记密码'},
    //     component: () => import ('@/view/login/forgetPass.vue')
    //   }, {
    //     path: '/forgetchange',
    //     name: 'forgetchange',
    //     meta: {title: '忘记密码选择方式'},
    //     component: () => import ('@/view/login/forgetchange.vue')
    //   }, {
    //     path: '/forgetchangeemail',
    //     name: 'forgetchangeemail',
    //     meta: {title: '忘记密码邮箱找回'},
    //     component: () => import ('@/view/login/forgetChangeEmail.vue')
    //   }
    //   ]
    // },
    ]
    },
  ]
},
    // 后台管理
    // 基础信息管理
    {
      path: '/backstage',
      name: 'backstage',
      meta: {title: '基础信息管理',
      routerIds: ['01']},
      redirect: 'backstage/department',
      component: () => import('@/view/backstage/index.vue'),
        children:[{
          path: 'department',
          name: 'department',
          meta: {title: '部门管理',
          routerIds: ['0101']},
          component: () => import('@/view/backstage/department/index.vue'),
        },
        {
          path: 'major',
          name: 'major',
          meta: {title: '专业管理',
          routerIds: ['0102']},
          component: () => import('@/view/backstage/department/major/index.vue'),
        },
        {
          path: 'addMajor',
          name: 'addMajor',
          meta: {title: '添加专业',
          routerIds: ['0102']},
          hidden:true,
          component: () => import('@/view/backstage/department/major/addMajor.vue'),
        },
        {
          path: 'basicTeach',
          name: 'basicTeach',
          meta: {title: '基础教学计划管理',
          routerIds: ['0103']},
          component: () => import('@/view/backstage/department/basicTeach/index.vue'),
        },
        {
          path: 'addPlan',
          name: 'addPlan',
          meta: {title: '添加计划',
          routerIds: ['0103']},
          hidden:true,
          component: () => import('@/view/backstage/department/basicTeach/addPlan.vue'),
        },
        {
          path: 'LinkTeach',
          name: 'LinkTeach',
          meta: {title: '关联基础教学计划',
          routerIds: ['0103']},
          hidden:true,
          component: () => import('@/view/backstage/department/basicTeach/LinkTeach.vue'),
        },
        {
          path: 'addLinkTeach',
          name: 'addLinkTeach',
          meta: {title: '添加基础教学计划',
          routerIds: ['0103']},
          hidden:true,
          component: () => import('@/view/backstage/department/basicTeach/addLinkTeach.vue'),
        },

        {
          path: 'publicLesson',
          name: 'publicLesson',
          meta: {title: '公共课管理',
          routerIds: ['0104']},
          component: () => import('@/view/backstage/department/publicLesson/index.vue'),
        },

        {
          path: 'addLesson',
          name: 'addLesson',
          meta: {title: '添加公共课',
          routerIds: ['0104']},
          hidden:true,
          component: () => import('@/view/backstage/department/publicLesson/addLesson.vue'),
        },
        {
          path: 'class',
          name: 'class',
          meta: {title: '班级管理',
          routerIds: ['0105']},
          component: () => import('@/view/backstage/department/class/index.vue'),
        },
        {
          path: 'addClass',
          name: 'addClass',
          meta: {title: '添加公班级',
          routerIds: ['0105']},
          hidden:true,
          component: () => import('@/view/backstage/department/class/addClass.vue'),
        },
        {
          path: 'student',
          name: 'student',
          meta: {title: '学生管理',
          routerIds: ['0106']},
          component: () => import('@/view/backstage/department/student/index.vue'),
        },
        {
          path: 'addStudent',
          name: 'addStudent',
          meta: {title: '添加学生',
          routerIds: ['0106']},
          hidden:true,
          component: () => import('@/view/backstage/department/student/addStudent.vue'),
        },
        {
          path: 'schoolBook',
          name: 'schoolBook',
          meta: {title: '校本库管理',
          routerIds: ['0107']},
          component: () => import('@/view/backstage/department/schoolBook/index.vue'),
        },
        {
          path: 'addBook',
          name: 'addBook',
          meta: {title: '添加信息',
          routerIds: ['0107']},
          hidden:true,
          component: () => import('@/view/backstage/department/schoolBook/addBook.vue'),
        },








        {
          path: 'table',
          name: 'table',
          meta: {title: '专业管理',
          routerIds: ['0102']},
          hidden:true,
          component: () => import('@/view/backstage/department/dragTable.vue'),
        },
        {
          path: 'editTable',
          name: 'editTable',
          meta: {title: '专业管理',
          routerIds: ['0102']},
          hidden:true,
          component: () => import('@/view/backstage/department/inlineEditTable.vue'),
        },
        ]
    },
    {
      path: '/jurisdiction',
      name: 'jurisdiction',
      meta: {title: '账号权限管理',
      routerIds: ['02']},
      redirect: 'jurisdiction/teachInfo',
      component: () => import('@/view/backstage/index.vue'),
        children:[
          {
            path: 'roleManagement',
            // component: _import('authorizationmanage/roleManagement'),
            component: () => import('@/view/backstage/jurisdiction/roleManagement/index.vue'),
            name: 'roleManagement',
            meta: {
              title: '角色管理',
              routerIds: ['0201']
            }
          },
          {
            path: 'addRole',
            // component: _import('authorizationmanage/roleManagement'),
            component: () => import('@/view/backstage/jurisdiction/roleManagement/addRole.vue'),
            name: 'addRole',
            hidden:true,
            meta: {
              title: '添加角色',
              routerIds: ['0201']
            }
          },
          {
          path: 'teachInfo',
          name: 'teachInfo',
          meta: {
            title: '教师信息管理',
            routerIds: ['0203']
          },
          component: () => import('@/view/backstage/jurisdiction/teacherInfo/index.vue'),
        },
        {
          path: 'addTeach',
          name: 'addTeach',
          meta: {
            title: '教师信息管理',
            routerIds: ['0203']
          },
          hidden:true,
          component: () => import('@/view/backstage/jurisdiction/teacherInfo/addTeach.vue'),
        },
        // {
        //   path: 'teachInfo1',
        //   name: 'teachInfo1',
        //   meta: {
        //     title: '权限部门管理',
        //     routerIds: ['0201']
        //   },
        //   component: () => import('@/view/backstage/jurisdiction/teacherInfo/index.vue'),
        // },

        {
          path: 'moduleManagement',
          component: () => import('@/view/backstage/jurisdiction/moduleManagement/index.vue'),
          name: 'moduleManagement',
          meta: {
            title: '模块管理',
            routerIds: ['0204']
          }
        },
        {
          path: 'buttonManagement',
          component: () => import('@/view/backstage/jurisdiction/buttonManagement/index.vue'),
          name: 'buttonManagement',
          meta: {
            title: '按钮管理',
            routerIds: ['0205']
          }
        },
        {
          path: 'userManagement',
          component: () => import('@/view/backstage/jurisdiction/userManagement/index.vue'),
          name: 'userManagement',
          meta: {
            title: '用户管理',
            routerIds: ['0206']
          }
        },
        {
          path: 'addUser',
          component: () => import('@/view/backstage/jurisdiction/userManagement/addUser.vue'),
          name: 'addUser',
          hidden:true,
          meta: {
            title: '添加用户',
            routerIds: ['0206']
          }
        },
      ]
    },
    // 系统管理
    {
      path: '/sysMenege',
      name: 'sysMenege',
      meta: {title: '系统管理',
      routerIds: ['03']},
      redirect: 'sysMenege/bookCoupon',
      component: () => import('@/view/backstage/index.vue'),
        children:[
          //征订流程管理
          {
            path: 'biddingProcess',
            name: 'biddingProcess',
            meta: {title: '征订流程管理',routerIds: ['0302']},
            component: () => import('@/view/backstage/sysMenege/biddingProcess/index.vue'),
          },
          //征订周期管理
          {
            path: 'biddingCycle',
            name: 'biddingCycle',
            meta: {title: '征订周期管理',routerIds: ['0301']},
            component: () => import('@/view/backstage/sysMenege/biddingCycle/index.vue'),
          },
          //添加征订周期管理
          {
            path: 'addBbiddingCycle',
            name: 'addBbiddingCycle',
            meta: {title: '添加征订周期',routerIds: ['0301']},
            hidden:true,
            component: () => import('@/view/backstage/sysMenege/biddingCycle/add.vue'),
          },

          
          //经销商管理
          {
            path: 'distributor',
            name: 'distributor',
            meta: {title: '经销商管理',routerIds: ['0303']},
            component: () => import('@/view/backstage/sysMenege/distributor/index.vue'),
          },
          //添加经销商
          {
            path: 'addDistributor',
            name: 'addDistributor',
            meta: {title: '添加经销商',routerIds: ['0303']},
            hidden:true,
            component: () => import('@/view/backstage/sysMenege/distributor/add.vue'),
          },
          //学校信息管理
          {
            path: 'school',
            name: 'school',
            meta: {title: '学校信息管理',routerIds: ['0308']},
            component: () => import('@/view/backstage/sysMenege/school/index.vue'),
          },
          // 样书券管理
          {
          path: 'bookCoupon',
          name: 'bookCoupon',
          meta: {title: '样书券管理',
          routerIds: ['0304']},
          component: () => import('@/view/backstage/sysMenege/bookCoupon/index.vue'),
          },
          {
            path: 'grantBook',
            name: 'grantBook',
            meta: {title: '样书券发放',
            routerIds: ['0304']},
            hidden:true,
            component: () => import('@/view/backstage/sysMenege/bookCoupon/grantBook.vue'),
          },
          // 样书申请
          {
            path: 'bookApply',
            name: 'bookApply',
            meta: {title: '样书申请管理',
            routerIds: ['0305']},
            component: () => import('@/view/backstage/sysMenege/bookApply/index.vue'),
          },
          {
            path: 'viewApply',
            name: 'viewApply',
            meta: {title: '查看样书申请',
            routerIds: ['0305']},
            hidden:true,
            component: () => import('@/view/backstage/sysMenege/bookApply/viewApply.vue'),
          },
          {
            path: 'mailManage',
            name: 'mailManage',
            meta: {title: '站内信息管理',
            routerIds: ['0306']},
            component: () => import('@/view/backstage/sysMenege/mailManage.vue'),
          },
          {
            path: 'addMail',
            name: 'addMail',
            meta: {title: '站内信息管理',
            routerIds: ['0306']},
            hidden:true,
            component: () => import('@/view/backstage/sysMenege/addMail.vue'),
          },
          {
          path: 'systomLog',
          name: 'systomLog',
          meta: {title: '日志管理',
          routerIds: ['0307']},
          component: () => import('@/view/backstage/sysMenege/systomLog.vue'),
          },
          {
            path: 'teachingPlan',
            name: 'teachingPlan',
            meta: {title: '实施性教学计划管理',
            routerIds: ['0309']},
            component: () => import('@/view/backstage/sysMenege/teachingPlan/index.vue'),
          },
          {
            path: 'dicManage',
            name: 'dicManage',
            meta: {title: '字典管理',
            routerIds: ['0310']},
            component: () => import('@/view/backstage/sysMenege/dicManage/index.vue'),
          },
          {
            path: 'addDic',
            name: 'addDic',
            meta: {title: '添加字典',
            routerIds: ['0310']},
            hidden:true,
            component: () => import('@/view/backstage/sysMenege/dicManage/addDic.vue'),
          },
        ]
    },
    // 支付设置
    {
      path: '/payMenege',
      name: 'payMenege',
      meta: {title: '支付管理',
      routerIds: ['05']},
      redirect: 'payMenege/paySet',
      component: () => import('@/view/backstage/index.vue'),
        children:[
          //支付设置
          {
            path: 'paySet',
            name: 'paySet',
            meta: {title: '支付设置',routerIds: ['0501']},
            component: () => import('@/view/backstage/payMenege/paySet.vue'),
          },
          //支付记录管理
          {
            path: 'payRecord',
            name: 'payRecord',
            meta: {title: '支付记录管理',routerIds: ['0502']},
            component: () => import('@/view/backstage/payMenege/payRecord.vue'),
          },


        ]
    },
  {
    path: '*',
    redirect: '/404',
    hidden: true
  }

]
