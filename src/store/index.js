import Vue from 'vue'
import Vuex from 'vuex'
import Cookies from 'js-cookie';

import app from './modules/app'
// import errorLog from './modules/errorLog'
import permission from './modules/permission'
import tagsView from './modules/tagsView'
import user from './modules/user'
import workbench from './modules/workbench'
import pageTemp from './modules/pageTemp';
import getters from './getters';


Vue.use(Vuex)
const state = {
  tips: '',
  isShowtip: false,
  issuggest:false,

  // userCompanyt:null,
  userFlow:null,
  searchActivity:null,
}

const store = new Vuex.Store({
  mutations: {

    tipsFun(state,n){
      state.tips=n;
      state.isShowtip=true;
      let self=this;
      window.setTimeout(function(){
        state.isShowtip=false;
      },2000)
    },
    suggest(state,n){
      state.issuggest=n;
    },
    /*更改二级分类*/
    changeType(state,n){
      state.curType=n;
    },
    changeTypeChild(state,n){
      state.curTypeChild=n;
    },
    changeCustomType(state,n){
      state.curCustomType=n;
    },

    // 活动列表搜索
    searchActivityList(state,n){
      state.searchActivity = n
    }

    },

    modules: {
      app,
      // errorLog,
      permission,
      tagsView,
      user,
      pageTemp,
      workbench
    },
    getters,
    state
})
export default store
