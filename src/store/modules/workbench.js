
const workbench = {
    state: {
        status: ''
    },
    mutations: {
        SET_QUERY_STATUS:(state,status) => {
            state.status = status
        }
    },
    actions: {
        setQueryStatus({commit},status){
            commit('SET_QUERY_STATUS',status)
        }
    }
  }
  
  export default workbench
  