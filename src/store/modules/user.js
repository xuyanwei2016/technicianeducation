import {  logout, login, getRouterList, getPageButtonList } from '@/api/login'
import { getToken, setToken, removeToken,getUserType } from '@/utils/auth'
import { getBiddingCycleAPI,getUserCompanytAPI } from '@/api/global'
import {logoutAPI} from '@/api/resources.js';
import {resetRouter} from '../../router';
import {baseUrl} from '../../utils/global'

import Cookies from 'js-cookie'

const user = {
    state: {
        user: '',
        status: '',
        token: getToken(),
        name: '',
        avatar: '',
        history:'',
        roles: [],
        pageBtn: {},
        routerList:[],
        userType:getUserType()||'school',//学校用户school，省监管用户supervise
        userName: Cookies.get("userName"),
        userImg: Cookies.get("userImg"),
        userToken: Cookies.get("Y-Token"),
        userPhone: Cookies.get("userPhone"),
        userCompanyt:null,
        userFlow:null,
        cycleTime:Cookies.get("cycleTime"),
    },

    mutations: {
        SET_STATUS: (state, status) => {
            state.status = status
        },
        SET_NAME: (state, name) => {
            state.name = name
        },
        SET_AVATAR: (state, avatar) => {
            state.avatar = avatar
        },
        SET_ROLES: (state, roles) => {
            state.roles = roles
        },
        SET_ROUTERLIST: (state, routers) => {
            state.routerList = routers
        },
        SET_PAGE_BTN: (state, code_btns) => {
            state.pageBtn[code_btns[0]] = code_btns[1]
        },
        LOGIN_OUT:(state,status)=>{
            state.userName=null;
            state.userImg=null;
            state.userToken=null;
            state.userPhone=null;
            state.userCompanyt = null;
            state.userFlow = null;
            state.cycleTime = null;
            state.history = ''
        },
        LOGIN:(state,n) =>{
            state.userName=n.nickName;
            state.userImg=n.img;
            state.userToken=n.token;
            state.userPhone=n.account;
        },
        GET_BIDDING_CYCLE:(state,data)=>{
            state.cycleTime=data
        },
        UPLOAD_IMG:(state,n)=>{
            state.userImg=n;
        },
        SET_USER_FLOW:(state,n)=>{
            state.userFlow=n;
        },
        SET_USER_COMPANYT:(state,n)=>{
            state.userCompanyt = n
        },
        HISTORY_ROUTER(state,n){
            state.history=n;
        },
        
    },

    actions: {

        LoginBtn({ commit},data) {
            return new Promise((resolve, reject) => {
                Cookies.set('userName', data.nickName);
                Cookies.set('userImg', data.img)
                Cookies.set('userPhone', data.account);
                Cookies.set('userType', 'school');
                commit('LOGIN', data);
                getBiddingCycleAPI().then(res=>{
                    if(res.data.status){
                        commit('GET_BIDDING_CYCLE',res.data.data)
                        Cookies.set("cycleTime",res.data.data);
                    }
                })

                resolve(0)

            })
        },
        //获取按钮btn列表
        GetPageButtonList({ commit, state }, pageCode) {
            return new Promise((resolve, reject) => {
                getPageButtonList(pageCode, state.token).then(res => {
                    commit('SET_PAGE_BTN', [pageCode, res.data.data])
                    resolve()
                }).catch(err => {

                })
            })
        },

        //获取路由列表
        GetRouterList({ commit, state }) {
            return new Promise((resolve, reject) => {
                commit('SET_ROLES', ['admin'])
                getRouterList().then(res => {
                    resolve(res)
                    
                    commit('SET_ROUTERLIST',res.data.data.mList)
                }).catch(error => {
                    reject(error)
                })
            })
        },
        // 获取用户登录单位信息
        GetUserCompany({commit}){
            // 获取登录用户单位信息
          getUserCompanytAPI().then(res => {
            if (res.data.code === 0) {
              commit('SET_USER_COMPANYT',res.data.data)
            } else {
              /*this.$message.error("456435464621")*/
              // this.$message.error(res.data.msg)
            }
          })
        },
        /*历史路径*/
        HistoryRouter({commit},n){
            commit('HISTORY_ROUTER',n)
        },

        /*修改用户头像*/
        UploadImg({commit},n){
            commit('UPLOAD_IMG',n)
        },
        //设置userFlow
        setUserFlow({commit},n){
            commit('SET_USER_FLOW',n)
        },


        /*退出登录*/
        LoginOut({commit,state}){
        return new Promise((resolve,reject) =>{
            logoutAPI().then(res=>{
                if(res.data.status){
                  Cookies.remove('Y-Token')
                  Cookies.remove('S-Token')
                  Cookies.remove('userName')
                  Cookies.remove('userImg')
                  Cookies.remove('userPhone')
                  Cookies.remove('userFlow');
                  Cookies.remove("cycleTime");
                  Cookies.remove('userFlowStatus');
                  Cookies.remove('flowCode');
                  Cookies.remove('flowDepth');
                  Cookies.remove('userType');
                  resetRouter()
                  commit('LOGIN_OUT','')
                  if(Cookies.get('userType') == 'supervise'){
                    let url = `${baseUrl}/supervisory/index.html#/login`
                    // let url = `http://192.168.2.29:8081/#/login`
                    // window.close();
                    window.open(url,'_self')
                  }else{
                    // let url = `http://192.168.2.29:8080/#/login`
                    let url = `${baseUrl}/fg/index.html#/index`
                    // window.close();
                    window.open(url,'_self')
                  }
                }
                resolve(res)
              }).catch(error =>{
                  reject(error)
              })
        })
        },

    }
}

export default user
