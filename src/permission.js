import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css'// progress bar style
import { getToken,getSToken,getUserType} from '@/utils/auth' // getToken from cookie
NProgress.configure({ showSpinner: false })// NProgress Configuration

// permissiom judge function
function hasPermission(roles, permissionRoles) {
  if (roles.indexOf('admin') >= 0) return true // admin permission passed directly
  if (!permissionRoles) return true
  return roles.some(role => permissionRoles.indexOf(role) >= 0)
}

const whiteList = ['/login','/index', '/searchDefault', '/materialType', '/promote', '/resources', '/resources/info','/text', '/resources/vocation','/resources/teaching',  '/noticeList', '/noticeIndex','/noticeIndex/noticeDetail', '/mechanicList', '/mechanicIndex','/mechanicIndex/mechanicDetail', '/stationList', '/stationIndex','/stationIndex/stationDetail', '/unionList', '/unionIndex','/unionIndex/unionDetail', '/forumList', '/forumIndex', '/forumIndex/forumDetail','/footerDetail', '/personal','/forgetIndex', '/forgetPass','/forgetchange', '/forgetchangeemail', '/','/404']// no redirect whitelist

router.beforeEach((to, from, next) => {
  NProgress.start() // start progress bar
  if (getToken()) { // determine if there has token
    console.log(1)
    if (to.path === '/studentPay') {
      console.log(2)
      next()
    } else if (to.path === '/personal'||to.path === '/personal/detail') {
      console.log(3)
      if(getUserType() =='supervise'){
        next({path: '/personal/evaluate'})
      }else{
        next()
      }
    }else if (to.path === '/index') {
      console.log(4)
      if(getUserType() =='supervise'){
        next({path: '/supervise'})
      }else{
        console.log(store.getters.roles,222)
        console.log(store.getters.addRouters)

          if (store.getters.roles.length == 0) { // 判断当前用户是否已拉取完user_info信息
            console.log('路由守卫')
            store.dispatch('GetRouterList').then(res => { // 拉取user_info
              const routerIds = res.data.data.mList // res.data.roles note: roles must be a array! such as: ['editor','develop']
              store.dispatch('GenerateRoutes', { routerIds }).then(() => { // 根据roles权限生成可访问的路由表
                router.addRoutes(store.getters.addRouters) // 动态添加可访问路由表
                // console.log(store.getters.addRouters)
                next({ ...to, replace: true }) // hack方法 确保addRoutes已完成 ,set the replace: true so the navigation will not leave a history record
              })
            }).catch((err) => {
              store.dispatch('LoginOut').then(() => {
                Message.error(err || 'Verification failed, please login again')
                next({ path: '/login' })
              })
            })
          } else {
              if (Object.prototype.toString.call(to.meta.routerIds) !== '[object Undefined]') {
                store.dispatch('GetPageButtonList', to.meta.routerIds[0]).then(() => {
                  next()
                })

            // }
            // // 没有动态改变权限的需求可直接next() 删除下方权限判断 ↓
          } else {
            next()
          }
          // // 可删 ↑
        }
      }
    }
    else if(getUserType() =='supervise'){
      console.log(5)
      next()
    }
    else {
      if (store.getters.roles.length == 0) { // 判断当前用户是否已拉取完user_info信息
        console.log('路由守卫')
        store.dispatch('GetRouterList').then(res => { // 拉取user_info
          const routerIds = res.data.data.mList // res.data.roles note: roles must be a array! such as: ['editor','develop']
          store.dispatch('GenerateRoutes', { routerIds }).then(() => { // 根据roles权限生成可访问的路由表
            router.addRoutes(store.getters.addRouters) // 动态添加可访问路由表
            // console.log(store.getters.addRouters)
            next({ ...to, replace: true }) // hack方法 确保addRoutes已完成 ,set the replace: true so the navigation will not leave a history record
          })
        }).catch((err) => {
          store.dispatch('LoginOut').then(() => {
            Message.error(err || 'Verification failed, please login again')
            next({ path: '/login' })
          })
        })
      } else {
        if (Object.prototype.toString.call(to.meta.routerIds) !== '[object Undefined]') {
          store.dispatch('GetPageButtonList', to.meta.routerIds[0]).then(() => {
            next()
          })
          // }
          // // 没有动态改变权限的需求可直接next() 删除下方权限判断 ↓
        } else {
          next()
        }
        // // 可删 ↑
      }
    }
  } else if(getSToken()){
    console.log(6)
    next()
  } else if(getUserType() =='supervise'){
    console.log(7)
    next()
  }

  else{
    console.log(8)
    /* has no token*/
    store.dispatch('HistoryRouter',from.fullPath);
    if (whiteList.indexOf(to.path) !== -1) { // 在免登录白名单，直接进入
      next()
    } else {
      console.log(9)
      next('/login') // 否则全部重定向到登录页
      NProgress.done() // if current page is login will not trigger afterEach hook, so manually handle it
    }
  }
})

router.afterEach(() => {
  NProgress.done() // finish progress bar
})
