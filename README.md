# y

> technician Education

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).



相关字段备注：
Y-Token：学校用户
S-token:学生用户
cycleTime：征订周期时间
flowCode：当前所属征订流程
flowDepth：征订流程深度
userFlowStatus：是否开启征订 0 开启
userType：登录用户类型   学校，省监管

recommendInfo：跳转资源推荐携带数据
recommendData：推荐页缓存